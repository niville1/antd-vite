# antd-vite

Project template with Ant design and vite

## Getting started

1. Clone the repo
2. Install dependencies
3. Start project with command

```bash
    npm run dev
```

## Where to find routes:

You'll find routes in the app routes file. All routes should be setup there.
Check funders form to see how to use ant design forms only.
