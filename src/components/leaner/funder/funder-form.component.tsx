import { Button, Form, Input, Select } from "antd";
import { useForm } from "antd/es/form/Form";
import React, { useState } from "react";

const branches = [
  {
    code: "01",
    description: "Branch 1",
  },
  {
    code: "02",
    description: "Branch 2",
  },
  {
    code: "03",
    description: "Branch 3",
  },
  {
    code: "04",
    description: "Branch 4",
  },
];
export const FunderForm: React.FC = () => {
  const [submitting, setSubmitting] = useState(false);
  const [form] = useForm();

  const onFinish = (values: any) => {
    setSubmitting(true);
    console.log("values: ", values);
    form.resetFields();
    setSubmitting(false);
  };

  return (
    <Form form={form} onFinish={onFinish} layout="vertical">
      <Form.Item
        name="branch"
        label="Select a branch"
        rules={[
          {
            required: true,
            message: "You must select a branch",
          },
        ]}
      >
        <Select>
          {branches.map((branch, index) => (
            <Select.Option key={index} value={branch.code}>
              {branch.description}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        name="description"
        label="Description"
        requiredMark
        rules={[
          {
            required: true,
            message: "Description is required",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Button
        type="primary"
        htmlType="submit"
        loading={submitting}
        disabled={submitting}
      >
        Submit
      </Button>
    </Form>
  );
};
