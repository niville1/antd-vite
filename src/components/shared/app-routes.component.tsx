import { createBrowserRouter } from "react-router-dom";
import App from "../../App";
import { ErrorPage } from "../../pages/error.page";
import { FundersPage } from "../../pages/learners/funder/funder.page";
import { WelcomePage } from "../../pages/welcome.page";

export const appRoutes = createBrowserRouter([
  {
    path: "/",
    errorElement: <ErrorPage />,
    element: <WelcomePage />,
  },
  {
    path: "/home",
    element: <App />,
  },
  {
    path: "/funders",
    element: <FundersPage />,
  },
]);
