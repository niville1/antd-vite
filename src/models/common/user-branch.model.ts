import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IUserBranch {
    tenent: string
    userCode: string
    branch: string
    hash: string
}

export interface IUserBranchState extends IBaseState {
    readonly userBranches: IUserBranch[]
    readonly userBranch: IUserBranch
}

export interface IUserBranchResponse extends IResponseBase {
    data: IUserBranch
}

export const emptyUserBranch: IUserBranch = {
    tenent: '',
    userCode: '',
    branch: '',
    hash: '',
}
