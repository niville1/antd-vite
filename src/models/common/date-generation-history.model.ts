import { IBaseState } from "../base-state.model";
import { IResponseBase } from "../response.base";
import { emptyDateGeneration, IDateGeneration } from "./date-generation.model";

export interface IDateGenerationHistory extends IDateGeneration {
}

export interface IDateGenerationHistoryState extends IBaseState {
  readonly dateGenerationHistories: IDateGeneration[];
  readonly dateGenerationHistory: IDateGeneration;
}

export const emptyDateGenerationHistory: IDateGenerationHistory = {
  ...emptyDateGeneration
}

export interface IDateGenerationHistoryResponse extends IResponseBase {
	data: IDateGenerationHistory
}