export interface IAccountingSession {
  tenant?: string;
  branch?: string;
  transDate: Date;
}
// TODO: Karl

export const emptyAccountingSession: IAccountingSession = {
  tenant: "",
  branch: "",
  transDate: new Date()
}