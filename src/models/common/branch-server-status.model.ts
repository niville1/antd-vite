import { IBaseState } from "../base-state.model";
import { IResponseBase } from "../response.base";
import { IBranch } from "./branch.model";
import { ICurrentUser } from "./current-user.model";
import { IServerStatus } from "./server-status.model";
import { IUserBranch } from "./user-branch.model";

export interface IBranchServerStatus {
  branch: string;
  branches: IBranch[];
  serverStatuses: IServerStatus[];
  currentUser: ICurrentUser;
  userBranches: IUserBranch[];
}

export interface IBranchServerStatusState extends IBaseState {
  readonly branchServerStatus: IBranchServerStatus;
}
export interface IBranchServerStatusResponse extends IResponseBase {
  data: IBranchServerStatus
}

