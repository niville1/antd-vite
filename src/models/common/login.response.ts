import { IResponseBase } from "../response.base";
import { ICurrentUser } from "./user.model";

export interface ILoginRespone extends IResponseBase {
    currentUser: ICurrentUser;
  }

  export interface IAccessTokenRespone extends IResponseBase {
    access_token: string;
    token_type: string;
    scope: string;
    expires_in:number;
  }