import * as Yup from "yup";

export interface IConfirmEmail {
  email: string;
  token: string;
}


