
export interface IRoute {
    path: string;
    private: boolean;
    exact: boolean;
    component: React.FC;
}