export interface ICurrentUser {
  userName: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  locale: string;
}

export const emptyUser: ICurrentUser = {
  userName: "",
  fullName: "",
  email: "",
  phoneNumber: "",
  locale: "",
};
