import { IServerStatus } from './server-status.model';
import { IResponseBase } from '../response.base';

export interface IServerStatusBase {
	tenant: string;
	branch: string;
	transYear: string;
	yearStart?: Date;
	yearEnd?: Date;
	status: string;
	sysTrans: string;
	dayStatus: string;
	cashTrans: string;
	backOfficeOnly: boolean;
	transRef: number;
}

export const emptyServerStatusBase: IServerStatusBase = {
	tenant: "",
	branch: "",
	transYear: "",
	yearStart: new Date(),
	yearEnd: new Date(),
	status: "",
	sysTrans: "",
	dayStatus: "",
	cashTrans: "",
	backOfficeOnly: true,
	transRef: 0
}
export interface IServerStatusResponse extends IResponseBase {
	data: IServerStatus
}

