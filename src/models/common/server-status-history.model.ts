import { IBaseState } from "../base-state.model";
import { IResponseBase } from "../response.base";
import { emptyServerStatus, IServerStatus } from "./server-status.model";

export interface IServerStatusHistory extends IServerStatus {
}

export interface IServerStatusHistoryState extends IBaseState {
  readonly serverStatusHistories: IServerStatusHistory[];
  readonly serverStatusHistory: IServerStatusHistory;
}

export const emptyServerStatusHistory: IServerStatusHistory = {
  ...emptyServerStatus,
}

export interface IServerStatusHistoryResponse extends IResponseBase {
	data: IServerStatusHistory
}