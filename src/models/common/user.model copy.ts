export interface IUserInfo {
  code: string;
  usrName: string;
  photoUrl: string;
}

export const emptyUserInfo: IUserInfo = {
  code: "",
  usrName: "",
  photoUrl: "",
};
