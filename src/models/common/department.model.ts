import { IBaseState } from "../base-state.model";
import { IResponseBase } from '../response.base';

export interface IDepartment {
  code: string;
  segment: string;
  description: string;
  tenant: string;
  createdOn: Date;
}

export interface IDepartmentState extends IBaseState {
  readonly departments: IDepartment[];
  readonly department: IDepartment;
}

export const emptyDepartment: IDepartment = {
  code: "",
  segment: "",
  description: "",
  tenant: "",
  createdOn: new Date(),
}
export interface IDepartmentResponse extends IResponseBase {
  data: IDepartment
}
