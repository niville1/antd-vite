import * as Yup from "yup";

export interface ILogin {
  userName: string;
  password: string;
}

export const emptyLogin = {
  userName: "",
  password: "",
};

export const loginValidationSchema = Yup.object().shape({
  userName: Yup.string().required("Username is required !"),
  password: Yup.string().required("Password is required !"),
});
