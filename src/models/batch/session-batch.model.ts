import { IBaseState } from "../base-state.model";
import { IBranch } from "../common/branch.model";
import { ICurrentUser } from "../common/current-user.model";
import { IDateGeneration } from "../common/date-generation.model";
import { IServerStatus } from "../common/server-status.model";
import { IUserMenu } from "../common/user-menu.model";
import { IUserInfo } from "../common/user.model copy";
export interface ISessionBatch {
  tenant: string;
  branch: string;
  branches: IBranch[];
  serverStatuses: IServerStatus[];
  dateGenerations: IDateGeneration[];
  currentUser: ICurrentUser;
  userMenus: IUserMenu;
  user: IUserInfo;
}

export interface ISessionBatchState extends IBaseState {
  readonly sessionBatch?: ISessionBatch;
}
