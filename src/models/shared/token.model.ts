export interface IToken {
  accessToken: string | null | undefined;
  email: string | null | undefined;
}
  
  export interface ITokenState {
    readonly isLoading: boolean;
    readonly token: IToken;
    readonly errors?: string | undefined;
    readonly initialFetch: boolean;
  }
  