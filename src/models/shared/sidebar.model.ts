import { ReactNode } from "react";

export interface ISideBar {
  app: string;
  code: string;
  description: string;
  route: string;
  icon?: ReactNode;
  dropDownMenus: ISideBar[];
  dropDown: boolean;
  parent: string;
}
