export interface IActiveTab {
    isActive: string
}
export const emptyActiveTab: IActiveTab = {
    isActive: '1',
}
export interface IActiveTabState {
    readonly activeTab: IActiveTab
}
