export interface ISideBarToggle {
  isOpen: boolean;
}

export interface ISideBarToggleState {
  readonly sideBarToggle: ISideBarToggle;
}
