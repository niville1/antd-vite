import { IBaseState } from "../base-state.model";
import { IResponseBase } from "../response.base";

export interface IUser {
  fullName: string;
  organization: string;
  Locale: string;
  userCode: string;
  imageUrl: string;
  email: string;
  emailConfirmed: boolean;
  phoneNumber: string;
  phoneNumberConfirmed: boolean;
  twoFactorEnabled: boolean;
}

export const emptyUser: IUser = {
  fullName: "",
  organization: "",
  Locale: "",
  userCode: "",
  imageUrl: "",
  email: "",
  emailConfirmed: false,
  phoneNumber: "",
  phoneNumberConfirmed: false,
  twoFactorEnabled: false,
}

export interface IUserResponse extends IResponseBase {
  data: IUser
}

export interface IUserState extends IBaseState {
    user: IUser;
    users: IUser[];
}
