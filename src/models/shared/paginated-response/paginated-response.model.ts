
export interface IPaginatedResponse<T> {
    pageNumber?: number;
    pageSize?: number;
    firstPage?: string;
    lastPage?: string;
    nextPage?: string;
    previousPage?: string;
    succeeded?: boolean;
    errors?: string;
    message?: string
    data: Array<T>;
  }