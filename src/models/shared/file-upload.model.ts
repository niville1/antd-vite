import { IBaseState } from "../base-state.model";

export interface IFileUpload {
  file?: File;
  fileName: string;
  branch: string;
  fileDescription: string;
}

export interface IFileUploadState extends IBaseState {
  readonly fileUploads: IFileUpload[];
  readonly fileUpload: IFileUpload;
}

export const emptyFileUpload: IFileUpload = {
  fileName: "",
  branch: "",
  fileDescription: "",
};
