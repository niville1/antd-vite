import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IInterventionType {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IInterventionTypeState extends IBaseState {
    readonly interventionTypes: IInterventionType[]
    readonly interventionType: IInterventionType
}

export const emptyInterventionType: IInterventionType = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IInterventionTypeResponse extends IResponseBase {
    data: IInterventionType
}
