import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IScreeningResponse {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IScreeningResponseState extends IBaseState {
    readonly screeningResponses: IScreeningResponse[]
    readonly screeningResponse: IScreeningResponse
}

export const emptyScreeningResponse: IScreeningResponse = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IScreeningResponseResponse extends IResponseBase {
    data: IScreeningResponse
}
