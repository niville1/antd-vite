import { IBaseState } from "../base-state.model"
import { IResponseBase } from "../response.base"


export interface IDifficultyLevel {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IDifficultyLevelState extends IBaseState {
    readonly difficultyLevels: IDifficultyLevel[]
    readonly difficultyLevel: IDifficultyLevel
}

export const emptyDifficultyLevel: IDifficultyLevel = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IDifficultyLevelResponse extends IResponseBase {
    data: IDifficultyLevel
}
