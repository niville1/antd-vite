import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IFunctionalLimitationGroup {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IFunctionalLimitationGroupState extends IBaseState {
    readonly functionalLimitationGroups: IFunctionalLimitationGroup[]
    readonly functionalLimitationGroup: IFunctionalLimitationGroup
}


export const emptyFunctionalLimitationGroup: IFunctionalLimitationGroup = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IFunctionalLimitationGroupResponse extends IResponseBase {
    data: IFunctionalLimitationGroup
}
