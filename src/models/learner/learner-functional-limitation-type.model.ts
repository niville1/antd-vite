import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface ILearnerFunctionalLimitationType {
    tenant: string
    school: string
    learner: string
    limitation: string
    limitationDate: Date
}

export interface ILearnerFunctionalLimitationTypeState extends IBaseState {
    readonly learnerFunctionalLimitationTypes: ILearnerFunctionalLimitationType[]
    readonly learnerFunctionalLimitationType: ILearnerFunctionalLimitationType
}

export const emptyLearnerFunctionalLimitationType: ILearnerFunctionalLimitationType = {
    tenant: '',
    school:'',
    learner: '',
    limitation: '',
    limitationDate: new Date(),
}

export interface ILearnerFunctionalLimitationTypeResponse extends IResponseBase {
    data: ILearnerFunctionalLimitationType
}
