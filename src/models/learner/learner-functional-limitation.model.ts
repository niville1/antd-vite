import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface ILearnerFunctionalLimitation {
    tenant: string
    learner: string
    limitation: string
    limitationDate: Date
}

export interface ILearnerFunctionalLimitationState extends IBaseState {
    readonly learnerFunctionalLimitations: ILearnerFunctionalLimitation[]
    readonly learnerFunctionalLimitation: ILearnerFunctionalLimitation
}

export const emptyLearnerFunctionalLimitation: ILearnerFunctionalLimitation = {
    tenant: '',
    learner: '',
    limitation: '',
    limitationDate: new Date(),
}

export interface ILearnerFunctionalLimitationResponse extends IResponseBase {
    data: ILearnerFunctionalLimitation
}
