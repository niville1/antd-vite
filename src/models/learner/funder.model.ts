import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IFunder {
    tenant: string
    code: string
    branch: string
    description: string
    createdOn: Date
}

export interface IFunderState extends IBaseState {
    readonly funders: IFunder[]
    readonly funder: IFunder
}

export const emptyFunder: IFunder = {
    tenant: '',
    code: '',
    branch:'',
    description: '',
    createdOn: new Date(),
}

export interface IFunderResponse extends IResponseBase {
    data: IFunder
}
