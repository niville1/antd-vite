import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IMedicalPersonnel {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IMedicalPersonnelState extends IBaseState {
    readonly medicalPersonnels: IMedicalPersonnel[]
    readonly medicalPersonnel: IMedicalPersonnel
}

export const emptyMedicalPersonnel: IMedicalPersonnel = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IMedicalPersonnelResponse extends IResponseBase {
    data: IMedicalPersonnel
}
