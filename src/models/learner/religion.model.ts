import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IReligion {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IReligionState extends IBaseState {
    readonly religions: IReligion[]
    readonly religion: IReligion
}

export const emptyReligion: IReligion = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IReligionResponse extends IResponseBase {
    data: IReligion
}
