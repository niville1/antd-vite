import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IFunctionalLimitation {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IFunctionalLimitationState extends IBaseState {
    readonly functionalLimitations: IFunctionalLimitation[]
    readonly functionalLimitation: IFunctionalLimitation
}


export const emptyFunctionalLimitation: IFunctionalLimitation = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IFunctionalLimitationResponse extends IResponseBase {
    data: IFunctionalLimitation
}
