import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface ILearnerFunctionalLimitationGroup {
    tenant: string
    learner: string
    school: string
    limitation: string
    limitationDate: Date
}

export interface ILearnerFunctionalLimitationGroupState extends IBaseState {
    readonly learnerFunctionalLimitationGroups: ILearnerFunctionalLimitationGroup[]
    readonly learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup
}

export const emptyLearnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup = {
    tenant: '',
    learner: '',
    school:'',
    limitation: '',
    limitationDate: new Date(),
}

export interface ILearnerFunctionalLimitationGroupResponse extends IResponseBase {
    data: ILearnerFunctionalLimitationGroup
}
