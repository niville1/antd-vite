import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface ILearnerInterventionType {
    tenant: string
    learner: string
    interventionType: string
    limitationDate: Date
}

export interface ILearnerInterventionTypeState extends IBaseState {
    readonly learnerInterventionTypes: ILearnerInterventionType[]
    readonly learnerInterventionType: ILearnerInterventionType
}

export const emptyLearnerInterventionType: ILearnerInterventionType = {
    tenant: '',
    learner: '',
    interventionType: '',
    limitationDate: new Date(),
}

export interface ILearnerInterventionTypeResponse extends IResponseBase {
    data: ILearnerInterventionType
}
