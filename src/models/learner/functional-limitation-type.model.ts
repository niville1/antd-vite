import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IFunctionalLimitationType {
    tenant: string
    code: string
    description: string
    createdOn: Date
}

export interface IFunctionalLimitationTypeState extends IBaseState {
    readonly functionalLimitationTypes: IFunctionalLimitationType[]
    readonly functionalLimitationType: IFunctionalLimitationType
}

export const emptyFunctionalLimitationType: IFunctionalLimitationType = {
    tenant: '',
    code: '',
    description: '',
    createdOn: new Date(),
}

export interface IFunctionalLimitationTypeResponse extends IResponseBase {
    data: IFunctionalLimitationType
}
