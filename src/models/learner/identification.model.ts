import { IBaseState } from '../base-state.model'
import { IResponseBase } from '../response.base'

export interface IIdentification {
    tenant: string
    code: string
    branch: string
    name: string
    surName: string
    gender: string
    birthDate: Date
}

export interface IIdentificationState extends IBaseState {
    readonly identifications: IIdentification[]
    readonly identification: IIdentification
}

export const emptyIdentification: IIdentification = {
    tenant: '',
    code: '',
    branch:'',
    name: '',
    surName:'',
    gender:'',
    birthDate: new Date(),
}

export interface IIdentificationResponse extends IResponseBase {
    data: IIdentification
}
