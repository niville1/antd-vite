import React from "react";
import { FunderForm } from "../../../components/leaner/funder/funder-form.component";

export const FundersPage: React.FC = () => {
  return (
    <div>
      <h3>Funders | Create new funder</h3>
      <FunderForm />
    </div>
  );
};
