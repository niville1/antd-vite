import React, { useState } from "react";
import { Button, MenuProps } from "antd";
import "../index.css";
import { css } from "@emotion/css";

export const WelcomePage: React.FC = () => {
  return (
    <div
      className={css`
        text-decoration: none;
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 1rem 2rem;

        a {
          padding-right: 1rem;
          text-decoration: none;
          color: #606060;
          transition: all 300ms ease-in-out;

          &:hover {
            color: #1677ff;
          }
        }
      `}
    >
      <a href="/">DamaDev</a>
      <nav
        className={css`
          display: flex;
          fiex-direction: column;
          width: 100%;
          @media screen and (min-width: 678px) {
            flex-direction: row;
            align-items: center;
            margin-left: auto;
          }
        `}
      >
        <a href="/funders">funders</a>
        <a href="/app">About</a>
        <a href="/app">Contact</a>
        <Button type="primary">Login</Button>
      </nav>
    </div>
  );
};
