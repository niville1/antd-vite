import { apiRequests } from "../../shared"
import { ILearnerFunctionalLimitation, ILearnerFunctionalLimitationResponse } from "../../../models/learner/learner-functional-limitation.model"

const learnerFunctionalLimitationService = {
	list: (): Promise<ILearnerFunctionalLimitation[]> => apiRequests.get('/learnerFunctionalLimitations'),
	details: (code: string): Promise<ILearnerFunctionalLimitation> => apiRequests.get(`/learnerFunctionalLimitations/${code}`),
	create: (learnerFunctionalLimitation: ILearnerFunctionalLimitation): Promise<ILearnerFunctionalLimitationResponse> =>
		apiRequests.post('/learnerFunctionalLimitations', learnerFunctionalLimitation),
	update: (learnerFunctionalLimitation: ILearnerFunctionalLimitation): Promise<ILearnerFunctionalLimitationResponse> => apiRequests.put('/learnerFunctionalLimitations', learnerFunctionalLimitation),
}

export default learnerFunctionalLimitationService
