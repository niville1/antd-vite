import { apiRequests } from "../../shared"
import { IScreeningResponse, IScreeningResponseResponse } from "../../../models/learner/screening-response.model"

const screeningResponseService = {
	list: (): Promise<IScreeningResponse[]> => apiRequests.get('/screeningResponses'),
	details: (code: string): Promise<IScreeningResponse> => apiRequests.get(`/screeningResponses/${code}`),
	create: (screeningResponse: IScreeningResponse): Promise<IScreeningResponseResponse> =>
		apiRequests.post('/screeningResponses', screeningResponse),
	update: (screeningResponse: IScreeningResponse): Promise<IScreeningResponseResponse> => apiRequests.put('/screeningResponses', screeningResponse),
}

export default screeningResponseService
