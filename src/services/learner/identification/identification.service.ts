import { apiRequests } from "../../shared"
import { IIdentification, IIdentificationResponse } from "../../../models/learner/identification.model"

const identificationService = {
	list: (): Promise<IIdentification[]> => apiRequests.get('/identifications'),
	details: (code: string): Promise<IIdentification> => apiRequests.get(`/identifications/${code}`),
	create: (identification: IIdentification): Promise<IIdentificationResponse> =>
		apiRequests.post('/identifications', identification),
	update: (identification: IIdentification): Promise<IIdentificationResponse> => apiRequests.put('/identifications', identification),
}

export default identificationService
