import { apiRequests } from "../../shared"
import { IFunctionalLimitationType, IFunctionalLimitationTypeResponse } from "../../../models/learner/functional-limitation-type.model"

const functionalLimitationTypeService = {
	list: (): Promise<IFunctionalLimitationType[]> => apiRequests.get('/functionalLimitationTypes'),
	details: (code: string): Promise<IFunctionalLimitationType> => apiRequests.get(`/functionalLimitationTypes/${code}`),
	create: (functionalLimitationType: IFunctionalLimitationType): Promise<IFunctionalLimitationTypeResponse> =>
		apiRequests.post('/functionalLimitationTypes', functionalLimitationType),
	update: (functionalLimitationType: IFunctionalLimitationType): Promise<IFunctionalLimitationTypeResponse> => apiRequests.put('/functionalLimitationTypes', functionalLimitationType),
}

export default functionalLimitationTypeService
