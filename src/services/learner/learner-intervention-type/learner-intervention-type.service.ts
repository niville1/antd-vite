import { apiRequests } from "../../shared"
import { ILearnerInterventionType, ILearnerInterventionTypeResponse } from "../../../models/learner/learner-intervention-type.model"

const learnerInterventionTypeService = {
	list: (): Promise<ILearnerInterventionType[]> => apiRequests.get('/learnerInterventionTypes'),
	details: (code: string): Promise<ILearnerInterventionType> => apiRequests.get(`/learnerInterventionTypes/${code}`),
	create: (learnerInterventionType: ILearnerInterventionType): Promise<ILearnerInterventionTypeResponse> =>
		apiRequests.post('/learnerInterventionTypes', learnerInterventionType),
	update: (learnerInterventionType: ILearnerInterventionType): Promise<ILearnerInterventionTypeResponse> => apiRequests.put('/learnerInterventionTypes', learnerInterventionType),
}

export default learnerInterventionTypeService
