
import { IDifficultyLevel, IDifficultyLevelResponse } from "../../../models/learner/difficulty-level.model"
import { apiRequests } from "../../shared"

const difficultyLevelService = {
	list: (): Promise<IDifficultyLevel[]> => apiRequests.get('/difficultyLevels'),
	details: (code: string): Promise<IDifficultyLevel> => apiRequests.get(`/difficultyLevels/${code}`),
	create: (difficultyLevel: IDifficultyLevel): Promise<IDifficultyLevelResponse> =>
		apiRequests.post('/difficultyLevels', difficultyLevel),
	update: (difficultyLevel: IDifficultyLevel): Promise<IDifficultyLevelResponse> => apiRequests.put('/difficultyLevels', difficultyLevel),
}

export default difficultyLevelService
