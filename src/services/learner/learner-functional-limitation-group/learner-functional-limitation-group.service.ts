import { apiRequests } from "../../shared"
import { ILearnerFunctionalLimitationGroup, ILearnerFunctionalLimitationGroupResponse } from "../../../models/learner/learner-functional-limitation-group.model"

const learnerFunctionalLimitationGroupService = {
	list: (): Promise<ILearnerFunctionalLimitationGroup[]> => apiRequests.get('/learnerFunctionalLimitationGroups'),
	details: (code: string): Promise<ILearnerFunctionalLimitationGroup> => apiRequests.get(`/learnerFunctionalLimitationGroups/${code}`),
	create: (learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup): Promise<ILearnerFunctionalLimitationGroupResponse> =>
		apiRequests.post('/learnerFunctionalLimitationGroups', learnerFunctionalLimitationGroup),
	update: (learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup): Promise<ILearnerFunctionalLimitationGroupResponse> => apiRequests.put('/learnerFunctionalLimitationGroups', learnerFunctionalLimitationGroup),
}

export default learnerFunctionalLimitationGroupService
