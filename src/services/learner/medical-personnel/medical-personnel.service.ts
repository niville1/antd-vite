import { apiRequests } from "../../shared"
import { IMedicalPersonnel, IMedicalPersonnelResponse } from "../../../models/learner/medical-personnel.model"

const medicalPersonnelService = {
	list: (): Promise<IMedicalPersonnel[]> => apiRequests.get('/medicalPersonnels'),
	details: (code: string): Promise<IMedicalPersonnel> => apiRequests.get(`/medicalPersonnels/${code}`),
	create: (medicalPersonnel: IMedicalPersonnel): Promise<IMedicalPersonnelResponse> =>
		apiRequests.post('/medicalPersonnels', medicalPersonnel),
	update: (medicalPersonnel: IMedicalPersonnel): Promise<IMedicalPersonnelResponse> => apiRequests.put('/medicalPersonnels', medicalPersonnel),
}

export default medicalPersonnelService
