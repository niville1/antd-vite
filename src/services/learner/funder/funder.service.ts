import { apiRequests } from "../../shared"
import { IFunder, IFunderResponse } from "../../../models/learner/funder.model"

const funderService = {
	list: (): Promise<IFunder[]> => apiRequests.get('/funders'),
	details: (code: string): Promise<IFunder> => apiRequests.get(`/funders/${code}`),
	create: (funder: IFunder): Promise<IFunderResponse> =>
		apiRequests.post('/funders', funder),
	update: (funder: IFunder): Promise<IFunderResponse> => apiRequests.put('/funders', funder),
}

export default funderService
