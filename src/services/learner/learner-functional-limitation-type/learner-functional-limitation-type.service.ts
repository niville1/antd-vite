import { apiRequests } from "../../shared"
import { ILearnerFunctionalLimitationType, ILearnerFunctionalLimitationTypeResponse } from "../../../models/learner/learner-functional-limitation-type.model"

const learnerFunctionalLimitationTypeService = {
	list: (): Promise<ILearnerFunctionalLimitationType[]> => apiRequests.get('/learnerFunctionalLimitationTypes'),
	details: (code: string): Promise<ILearnerFunctionalLimitationType> => apiRequests.get(`/learnerFunctionalLimitationTypes/${code}`),
	create: (learnerFunctionalLimitationType: ILearnerFunctionalLimitationType): Promise<ILearnerFunctionalLimitationTypeResponse> =>
		apiRequests.post('/learnerFunctionalLimitationTypes', learnerFunctionalLimitationType),
	update: (learnerFunctionalLimitationType: ILearnerFunctionalLimitationType): Promise<ILearnerFunctionalLimitationTypeResponse> => apiRequests.put('/learnerFunctionalLimitationTypes', learnerFunctionalLimitationType),
}

export default learnerFunctionalLimitationTypeService
