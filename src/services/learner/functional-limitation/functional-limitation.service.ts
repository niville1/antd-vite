
import { IFunctionalLimitation, IFunctionalLimitationResponse } from "../../../models/learner/functional-limitation.model"
import { apiRequests } from "../../shared"

const functionalLimitationService = {
	list: (): Promise<IFunctionalLimitation[]> => apiRequests.get('/functionalLimitations'),
	details: (code: string): Promise<IFunctionalLimitation> => apiRequests.get(`/functionalLimitations/${code}`),
	create: (functionalLimitation: IFunctionalLimitation): Promise<IFunctionalLimitationResponse> =>
		apiRequests.post('/functionalLimitations', functionalLimitation),
	update: (functionalLimitation: IFunctionalLimitation): Promise<IFunctionalLimitationResponse> => apiRequests.put('/functionalLimitations', functionalLimitation),
}

export default functionalLimitationService
