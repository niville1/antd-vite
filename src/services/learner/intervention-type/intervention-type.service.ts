import { apiRequests } from "../../shared"
import { IInterventionType, IInterventionTypeResponse } from "../../../models/learner/intervention-type.model"

const interventionTypeService = {
	list: (): Promise<IInterventionType[]> => apiRequests.get('/interventionTypes'),
	details: (code: string): Promise<IInterventionType> => apiRequests.get(`/interventionTypes/${code}`),
	create: (interventionType: IInterventionType): Promise<IInterventionTypeResponse> =>
		apiRequests.post('/interventionTypes', interventionType),
	update: (interventionType: IInterventionType): Promise<IInterventionTypeResponse> => apiRequests.put('/interventionTypes', interventionType),
}

export default interventionTypeService
