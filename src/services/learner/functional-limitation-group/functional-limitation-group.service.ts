
import { IFunctionalLimitationGroup, IFunctionalLimitationGroupResponse } from "../../../models/learner/functional-limitation-group.model"
import { apiRequests } from "../../shared"

const functionalLimitationGroupService = {
	list: (): Promise<IFunctionalLimitationGroup[]> => apiRequests.get('/functionalLimitationGroups'),
	details: (code: string): Promise<IFunctionalLimitationGroup> => apiRequests.get(`/functionalLimitationGroups/${code}`),
	create: (functionalLimitationGroup: IFunctionalLimitationGroup): Promise<IFunctionalLimitationGroupResponse> =>
		apiRequests.post('/functionalLimitationGroups', functionalLimitationGroup),
	update: (functionalLimitationGroup: IFunctionalLimitationGroup): Promise<IFunctionalLimitationGroupResponse> => apiRequests.put('/functionalLimitationGroups', functionalLimitationGroup),
}

export default functionalLimitationGroupService
