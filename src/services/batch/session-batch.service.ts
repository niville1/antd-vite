import { ISessionBatch } from "../../models/batch/session-batch.model";
import { apiRequests } from "../shared";

const sessionBatchService = {
    getBatch: (): Promise<ISessionBatch> => apiRequests.get("/sessionBatch"),
    getBatchBranch: (branch: string): Promise<ISessionBatch> => apiRequests.get(`/sessionBatch/${branch}`),
};

export {
    sessionBatchService
};
