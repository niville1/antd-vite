import { apiRequests } from '../../shared'
import {
    IUserBranch,
    IUserBranchResponse,
} from '../../../models/common/user-branch.model'

const userBranchService = {
    list: (): Promise<IUserBranch[]> => apiRequests.get('/userBranches'),
    details: (code: string): Promise<IUserBranch> =>
        apiRequests.get(`/userBranches/${code}`),
    create: (userBranch: IUserBranch): Promise<IUserBranchResponse> =>
        apiRequests.post('/userBranches', userBranch),
    update: (userBranch: IUserBranch): Promise<IUserBranchResponse> =>
        apiRequests.put('/userBranches', userBranch),
}

export default userBranchService
