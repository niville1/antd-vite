import { ICurrentBranch, ICurrentBranchResponse } from "../../../models/common/current-branch.model";
import { apiRequests } from "../../shared";

const currentBranchService = {
  list: (): Promise<ICurrentBranch[]> => apiRequests.get("/currentBranches"),
  details: (code: string): Promise<ICurrentBranch> =>
    apiRequests.get(`/currentBranches/${code}`),
  create: (currentBranch: ICurrentBranch): Promise<ICurrentBranchResponse> =>
    apiRequests.post("/currentBranches", currentBranch),
  update: (currentBranch: ICurrentBranch): Promise<ICurrentBranchResponse> =>
    apiRequests.put("/currentBranches", currentBranch),
};

export default currentBranchService;
