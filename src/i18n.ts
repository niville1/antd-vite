import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { enBranchStation } from './translations/common/branch-station/branch-station.en'
import { frBranchStation } from './translations/common/branch-station/branch-station.fr'
import { enBranch } from './translations/common/branch/branch.en'
import { frBranch } from './translations/common/branch/branch.fr'
import { enContactRelation } from './translations/common/contact-relation/contact-relation.en'
import { frContactRelation } from './translations/common/contact-relation/contact-relation.fr'
import { enDepartment } from './translations/common/department/department.en'
import { frDepartment } from './translations/common/department/department.fr'
import { enDivision } from './translations/common/division/division.en'
import { frDivision } from './translations/common/division/division.fr'
import { enReligion } from './translations/common/religion/religion.en'
import { frReligion } from './translations/common/religion/religion.fr'
import { enSource } from './translations/common/source/source.en'
import { frSource } from './translations/common/source/source.fr'
import { enStationReference } from './translations/common/station-reference/station-reference.en'
import { frStationReference } from './translations/common/station-reference/station-reference.fr'
import { enSubDivision } from './translations/common/sub-division/sub-division.en'
import { frSubDivision } from './translations/common/sub-division/sub-division.fr'
import { enTableHeaders } from './translations/common/table-headers/table-headers.en'
import { frTableHeaders } from './translations/common/table-headers/table-headers.fr'
import { enLocation } from './translations/common/location/location.en'
import { frLocation } from './translations/common/location/location.fr'
import { enSegment } from './translations/common/segment/segment.en'
import { frSegment } from './translations/common/segment/segment.fr'
import { enDifficultyLevel } from './translations/learner/difficulty-level/difficulty-level.en'
import { enFunctionalLimitation } from './translations/learner/functional-limitation/functional-limitation.en'
import { enFunctionalLimitationGroup } from './translations/learner/functional-limitation-group/functional-limitation-group.en'
import { enFunctionalLimitationType } from './translations/learner/functional-limitation-type/functional-limitation-type.en'
import { enFunder } from './translations/learner/funder/funder.en'
import { enIdentification } from './translations/learner/identification/identification.en'
import { enInterventionType } from './translations/learner/intervention-type/intervention-type.en'
import { enLearnerFunctionalLimitation } from './translations/learner/learner-functional-limitation/learner-functional-limitation.en'
import { enLearnerFunctionalLimitationType } from './translations/learner/learner-functional-limitation-type/learner-functional-limitation-type.en'
import { enLearnerFunctionalLimitationGroup } from './translations/learner/learner-functional-limitation-group/learner-functional-limitation-group.en'
import { enMedicalPersonnel } from './translations/learner/medical-personnel/medical-personnel.en'
import { enScreeningResponse } from './translations/learner/screening-response/screening-response.en'
import { frDifficultyLevel } from './translations/learner/difficulty-level/difficulty-level.fr'
import { frScreeningResponse } from './translations/learner/screening-response/screening-response.fr'
import { frMedicalPersonnel } from './translations/learner/medical-personnel/medical-personnel.fr'
import { frLearnerFunctionalLimitationGroup } from './translations/learner/learner-functional-limitation-group/learner-functional-limitation-group.fr'
import { frLearnerFunctionalLimitationType } from './translations/learner/learner-functional-limitation-type/learner-functional-limitation-type.fr'
import { frLearnerFunctionalLimitation } from './translations/learner/learner-functional-limitation/learner-functional-limitation.fr'
import { frInterventionType } from './translations/learner/intervention-type/intervention-type.fr'
import { frIdentification } from './translations/learner/identification/identification.fr'
import { frFunctionalLimitationType } from './translations/learner/functional-limitation-type/functional-limitation-type.fr'
import { frFunder } from './translations/learner/funder/funder.fr'
import { frFunctionalLimitationGroup } from './translations/learner/functional-limitation-group/functional-limitation-group.fr'
import { frFunctionalLimitation } from './translations/learner/functional-limitation/functional-limitation.fr'

const resources = {
    en: {
        translation: {
            appTitle: 'Inclusive Education',
            Profile: 'User profile',
        },
        common: {
            ...enTableHeaders,
            religion: {
                ...enReligion,
            },
            contact_relation: {
                ...enContactRelation,
            },
            division: {
                ...enDivision,
            },
            branch: {
                ...enBranch,
            },
            source: {
                ...enSource,
            },
            stationReference: {
                ...enStationReference,
            },
            branchStation: {
                ...enBranchStation,
            },
            department: {
                ...enDepartment,
            },
            sub_division: {
                ...enSubDivision,
            },
            location: {
                ...enLocation,
            },
            segment: {
                ...enSegment,
            },
        },
        learner:{
           difficultyLevel:{
            ...enDifficultyLevel,
           },
           functionalLimitation:{
            ...enFunctionalLimitation,
           },
           functionalLimitationGroup:{
            ...enFunctionalLimitationGroup,
           },
           functionalLimitationType:{
            ...enFunctionalLimitationType,
           },
           funder:{
            ...enFunder
           },
           identification:{
            ...enIdentification
           },
           interventionType:{
            ...enInterventionType
           },
           learnerFunctionalLimitation:{
            ...enLearnerFunctionalLimitation
           },
           learnerFunctionalLimitationType:{
            ...enLearnerFunctionalLimitationType
           },
           learnerFunctionalLimitationGroup:{
            ...enLearnerFunctionalLimitationGroup
           },
           medicalPersonnel:{
            ...enMedicalPersonnel
           },
           screeningresponse:{
            ...enScreeningResponse
           }
        }
    },
    fr: {
        translation: {
            appTitle: 'Inclusive Education',
            Profile: 'fr profile',
        },
        common: {
            ...frTableHeaders,
            religion: {
                ...frReligion,
            },
            contact_relation: {
                ...frContactRelation,
            },
            division: {
                ...frDivision,
            },
            branch: {
                ...frBranch,
            },
            source: {
                ...frSource,
            },
            stationReference: {
                ...frStationReference,
            },
            branchStation: {
                ...frBranchStation,
            },
            department: {
                ...frDepartment,
            },
            sub_division: {
                ...frSubDivision,
            },
            location: {
                ...frLocation,
            },
            segment: {
                ...frSegment,
            },
        },

        learner:{
            difficultyLevel:{
             ...frDifficultyLevel,
            },
            functionalLimitation:{
             ...frFunctionalLimitation,
            },
            functionalLimitationGroup:{
             ...frFunctionalLimitationGroup,
            },
            functionalLimitationType:{
             ...frFunctionalLimitationType,
            },
            funder:{
             ...frFunder
            },
            identification:{
             ...frIdentification
            },
            interventionType:{
             ...frInterventionType
            },
            learnerFunctionalLimitation:{
             ...frLearnerFunctionalLimitation
            },
            learnerFunctionalLimitationType:{
             ...frLearnerFunctionalLimitationType
            },
            learnerFunctionalLimitationGroup:{
             ...frLearnerFunctionalLimitationGroup
            },
            medicalPersonnel:{
             ...frMedicalPersonnel
            },
            screeningresponse:{
             ...frScreeningResponse
            }
         }

    },
}

i18n.use(initReactI18next).init({
    interpolation: {
        escapeValue: false,
    },
    lng: 'en',
    ns: ['ns1', 'ns2'],
    resources,
})

export default i18n
