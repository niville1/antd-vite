import React from "react";
import ReactDOM from "react-dom/client";
import store from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
import { Provider } from "react-redux";
import AuthProvider from "./utils/auth-provider";
import userManager from "./services/shared/auth/auth.service";

import "./index.css";
import { RouterProvider } from "react-router-dom";
import { appRoutes } from "./components/shared/app-routes.component";

let persistor = persistStore(store);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AuthProvider userManager={userManager} store={store}>
          <RouterProvider router={appRoutes} />
        </AuthProvider>
      </PersistGate>
    </Provider>
  </React.StrictMode>
);
