import App from "../App";
import { WelcomePage } from "../pages/welcome.page";

export const commonRoutes = [
  {
    path: "/",
    element: { WelcomePage },
  },
  {
    path: "/home",
    element: { App },
  },
];
