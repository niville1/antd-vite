import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as difficultyLevelActions from '../../../redux/learner/difficulty-level/difficulty-level.slice'
import { IDifficultyLevel } from '../../../models/learner/difficulty-level.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import difficultyLevelService from '../../../services/learner/difficulty-level/difficulty-level.service'

const useDifficultyLevel = () => {
    const difficultyLevels = useSelector<rootState, IDifficultyLevel[]>(
        (state) => state.difficultyLevel.difficultyLevels
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.difficultyLevel.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.difficultyLevel.initialFetch
    )
    const difficultyLevel = useSelector<rootState, IDifficultyLevel>((state) => state.difficultyLevel.difficultyLevel)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.difficultyLevel.updateMode
    )

    const dispatch = useDispatch()

    const loadDifficultyLevels = useCallback(() => {
        if (initialFetch) {
            dispatch(difficultyLevelActions.fetchDifficultyLevelsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addDifficultyLevel = async (difficultyLevel: IDifficultyLevel) => {
        return await difficultyLevelService
            .create(difficultyLevel)
            .then((difficultyLevelResponse) => {
                if (difficultyLevelResponse.success) {
                    dispatch(difficultyLevelActions.addDifficultyLevelSuccess(difficultyLevelResponse.data))
                } else {
                    return difficultyLevelResponse
                }
                return difficultyLevelResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setDifficultyLevel = (difficultyLevel: IDifficultyLevel) => {
        dispatch(difficultyLevelActions.setActiveDifficultyLevel(difficultyLevel))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(difficultyLevelActions.setDifficultyLevelUpdateMode(updateMode))
    }

    const editDifficultyLevel = async (difficultyLevel: IDifficultyLevel) => {
        return await difficultyLevelService
            .update(difficultyLevel)
            .then((difficultyLevelResponse) => {
                dispatch(difficultyLevelActions.editDifficultyLevelSuccess(difficultyLevelResponse.data))
                setDifficultyLevel(difficultyLevelResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveDifficultyLevel = async (difficultyLevel: IDifficultyLevel, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addDifficultyLevel(difficultyLevel)
            : await editDifficultyLevel(difficultyLevel)
    }

    useEffect(() => {
        loadDifficultyLevels()
    }, [difficultyLevel, difficultyLevels, isLoading, initialFetch, loadDifficultyLevels])

    return {
        difficultyLevel,
        difficultyLevels,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addDifficultyLevel,
        editDifficultyLevel,
        saveDifficultyLevel,
        setDifficultyLevel,
    }
}

export { useDifficultyLevel }
