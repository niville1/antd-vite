import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as interventionTypeActions from '../../../redux/learner/intervention-type/intervention-type.slice'
import { IInterventionType } from '../../../models/learner/intervention-type.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import interventionTypeService from '../../../services/learner/intervention-type/intervention-type.service'

const useInterventionType = () => {
    const interventionTypes = useSelector<rootState, IInterventionType[]>(
        (state) => state.interventionType.interventionTypes
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.interventionType.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.interventionType.initialFetch
    )
    const interventionType = useSelector<rootState, IInterventionType>((state) => state.interventionType.interventionType)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.interventionType.updateMode
    )

    const dispatch = useDispatch()

    const loadInterventionTypes = useCallback(() => {
        if (initialFetch) {
            dispatch(interventionTypeActions.fetchInterventionTypesAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addInterventionType = async (interventionType: IInterventionType) => {
        return await interventionTypeService
            .create(interventionType)
            .then((interventionTypeResponse) => {
                if (interventionTypeResponse.success) {
                    dispatch(interventionTypeActions.addInterventionTypeSuccess(interventionTypeResponse.data))
                } else {
                    return interventionTypeResponse
                }
                return interventionTypeResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setInterventionType = (interventionType: IInterventionType) => {
        dispatch(interventionTypeActions.setActiveInterventionType(interventionType))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(interventionTypeActions.setInterventionTypeUpdateMode(updateMode))
    }

    const editInterventionType = async (interventionType: IInterventionType) => {
        return await interventionTypeService
            .update(interventionType)
            .then((interventionTypeResponse) => {
                dispatch(interventionTypeActions.editInterventionTypeSuccess(interventionTypeResponse.data))
                setInterventionType(interventionTypeResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveInterventionType = async (interventionType: IInterventionType, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addInterventionType(interventionType)
            : await editInterventionType(interventionType)
    }

    useEffect(() => {
        loadInterventionTypes()
    }, [interventionType, interventionTypes, isLoading, initialFetch, loadInterventionTypes])

    return {
        interventionType,
        interventionTypes,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addInterventionType,
        editInterventionType,
        saveInterventionType,
        setInterventionType,
    }
}

export { useInterventionType }
