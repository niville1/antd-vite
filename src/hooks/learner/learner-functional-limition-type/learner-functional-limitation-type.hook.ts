import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as learnerFunctionalLimitationTypeActions from '../../../redux/learner/learner-functional-limitation-type/learner-functional-limition-type.slice'
import { ILearnerFunctionalLimitationType } from '../../../models/learner/learner-functional-limitation-type.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import learnerFunctionalLimitationTypeService from '../../../services/learner/learner-functional-limitation-type/learner-functional-limitation-type.service'

const useLearnerFunctionalLimitationType = () => {
    const learnerFunctionalLimitationTypes = useSelector<rootState, ILearnerFunctionalLimitationType[]>(
        (state) => state.learnerFunctionalLimitationType.learnerFunctionalLimitationTypes
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.learnerFunctionalLimitationType.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.learnerFunctionalLimitationType.initialFetch
    )
    const learnerFunctionalLimitationType = useSelector<rootState, ILearnerFunctionalLimitationType>((state) => state.learnerFunctionalLimitationType.learnerFunctionalLimitationType)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.learnerFunctionalLimitationType.updateMode
    )

    const dispatch = useDispatch()

    const loadLearnerFunctionalLimitationTypes = useCallback(() => {
        if (initialFetch) {
            dispatch(learnerFunctionalLimitationTypeActions.fetchLearnerFunctionalLimitationTypesAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addLearnerFunctionalLimitationType = async (learnerFunctionalLimitationType: ILearnerFunctionalLimitationType) => {
        return await learnerFunctionalLimitationTypeService
            .create(learnerFunctionalLimitationType)
            .then((learnerFunctionalLimitationTypeResponse) => {
                if (learnerFunctionalLimitationTypeResponse.success) {
                    dispatch(learnerFunctionalLimitationTypeActions.addLearnerFunctionalLimitationTypeSuccess(learnerFunctionalLimitationTypeResponse.data))
                } else {
                    return learnerFunctionalLimitationTypeResponse
                }
                return learnerFunctionalLimitationTypeResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setLearnerFunctionalLimitationType = (learnerFunctionalLimitationType: ILearnerFunctionalLimitationType) => {
        dispatch(learnerFunctionalLimitationTypeActions.setActiveLearnerFunctionalLimitationType(learnerFunctionalLimitationType))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(learnerFunctionalLimitationTypeActions.setLearnerFunctionalLimitationTypeUpdateMode(updateMode))
    }

    const editLearnerFunctionalLimitationType = async (learnerFunctionalLimitationType: ILearnerFunctionalLimitationType) => {
        return await learnerFunctionalLimitationTypeService
            .update(learnerFunctionalLimitationType)
            .then((learnerFunctionalLimitationTypeResponse) => {
                dispatch(learnerFunctionalLimitationTypeActions.editLearnerFunctionalLimitationTypeSuccess(learnerFunctionalLimitationTypeResponse.data))
                setLearnerFunctionalLimitationType(learnerFunctionalLimitationTypeResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveLearnerFunctionalLimitationType = async (learnerFunctionalLimitationType: ILearnerFunctionalLimitationType, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addLearnerFunctionalLimitationType(learnerFunctionalLimitationType)
            : await editLearnerFunctionalLimitationType(learnerFunctionalLimitationType)
    }

    useEffect(() => {
        loadLearnerFunctionalLimitationTypes()
    }, [learnerFunctionalLimitationType, learnerFunctionalLimitationTypes, isLoading, initialFetch, loadLearnerFunctionalLimitationTypes])

    return {
        learnerFunctionalLimitationType,
        learnerFunctionalLimitationTypes,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addLearnerFunctionalLimitationType,
        editLearnerFunctionalLimitationType,
        saveLearnerFunctionalLimitationType,
        setLearnerFunctionalLimitationType,
    }
}

export { useLearnerFunctionalLimitationType }
