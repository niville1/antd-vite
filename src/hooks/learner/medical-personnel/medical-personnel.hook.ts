import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as medicalPersonnelActions from '../../../redux/learner/medical-personnel/medical-personnel.slice'
import { IMedicalPersonnel } from '../../../models/learner/medical-personnel.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import medicalPersonnelService from '../../../services/learner/medical-personnel/medical-personnel.service'

const useMedicalPersonnel = () => {
    const medicalPersonnels = useSelector<rootState, IMedicalPersonnel[]>(
        (state) => state.medicalPersonnel.medicalPersonnels
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.medicalPersonnel.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.medicalPersonnel.initialFetch
    )
    const medicalPersonnel = useSelector<rootState, IMedicalPersonnel>((state) => state.medicalPersonnel.medicalPersonnel)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.medicalPersonnel.updateMode
    )

    const dispatch = useDispatch()

    const loadMedicalPersonnels = useCallback(() => {
        if (initialFetch) {
            dispatch(medicalPersonnelActions.fetchMedicalPersonnelsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addMedicalPersonnel = async (medicalPersonnel: IMedicalPersonnel) => {
        return await medicalPersonnelService
            .create(medicalPersonnel)
            .then((medicalPersonnelResponse) => {
                if (medicalPersonnelResponse.success) {
                    dispatch(medicalPersonnelActions.addMedicalPersonnelSuccess(medicalPersonnelResponse.data))
                } else {
                    return medicalPersonnelResponse
                }
                return medicalPersonnelResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setMedicalPersonnel = (medicalPersonnel: IMedicalPersonnel) => {
        dispatch(medicalPersonnelActions.setActiveMedicalPersonnel(medicalPersonnel))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(medicalPersonnelActions.setMedicalPersonnelUpdateMode(updateMode))
    }

    const editMedicalPersonnel = async (medicalPersonnel: IMedicalPersonnel) => {
        return await medicalPersonnelService
            .update(medicalPersonnel)
            .then((medicalPersonnelResponse) => {
                dispatch(medicalPersonnelActions.editMedicalPersonnelSuccess(medicalPersonnelResponse.data))
                setMedicalPersonnel(medicalPersonnelResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveMedicalPersonnel = async (medicalPersonnel: IMedicalPersonnel, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addMedicalPersonnel(medicalPersonnel)
            : await editMedicalPersonnel(medicalPersonnel)
    }

    useEffect(() => {
        loadMedicalPersonnels()
    }, [medicalPersonnel, medicalPersonnels, isLoading, initialFetch, loadMedicalPersonnels])

    return {
        medicalPersonnel,
        medicalPersonnels,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addMedicalPersonnel,
        editMedicalPersonnel,
        saveMedicalPersonnel,
        setMedicalPersonnel,
    }
}

export { useMedicalPersonnel }
