import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as funderActions from '../../../redux/learner/funder/funder.slice'
import { IFunder } from '../../../models/learner/funder.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import funderService from '../../../services/learner/funder/funder.service'

const useFunder = () => {
    const funders = useSelector<rootState, IFunder[]>(
        (state) => state.funder.funders
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.funder.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.funder.initialFetch
    )
    const funder = useSelector<rootState, IFunder>((state) => state.funder.funder)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.funder.updateMode
    )

    const dispatch = useDispatch()

    const loadFunders = useCallback(() => {
        if (initialFetch) {
            dispatch(funderActions.fetchFundersAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addFunder = async (funder: IFunder) => {
        return await funderService
            .create(funder)
            .then((funderResponse) => {
                if (funderResponse.success) {
                    dispatch(funderActions.addFunderSuccess(funderResponse.data))
                } else {
                    return funderResponse
                }
                return funderResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setFunder = (funder: IFunder) => {
        dispatch(funderActions.setActiveFunder(funder))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(funderActions.setFunderUpdateMode(updateMode))
    }

    const editFunder = async (funder: IFunder) => {
        return await funderService
            .update(funder)
            .then((funderResponse) => {
                dispatch(funderActions.editFunderSuccess(funderResponse.data))
                setFunder(funderResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveFunder = async (funder: IFunder, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addFunder(funder)
            : await editFunder(funder)
    }

    useEffect(() => {
        loadFunders()
    }, [funder, funders, isLoading, initialFetch, loadFunders])

    return {
        funder,
        funders,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addFunder,
        editFunder,
        saveFunder,
        setFunder,
    }
}

export { useFunder }
