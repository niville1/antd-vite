import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as identificationActions from '../../../redux/learner/identification/identification.slice'
import { IIdentification } from '../../../models/learner/identification.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import identificationService from '../../../services/learner/identification/identification.service'

const useIdentification = () => {
    const identifications = useSelector<rootState, IIdentification[]>(
        (state) => state.identification.identifications
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.identification.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.identification.initialFetch
    )
    const identification = useSelector<rootState, IIdentification>((state) => state.identification.identification)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.identification.updateMode
    )

    const dispatch = useDispatch()

    const loadIdentifications = useCallback(() => {
        if (initialFetch) {
            dispatch(identificationActions.fetchIdentificationsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addIdentification = async (identification: IIdentification) => {
        return await identificationService
            .create(identification)
            .then((identificationResponse) => {
                if (identificationResponse.success) {
                    dispatch(identificationActions.addIdentificationSuccess(identificationResponse.data))
                } else {
                    return identificationResponse
                }
                return identificationResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setIdentification = (identification: IIdentification) => {
        dispatch(identificationActions.setActiveIdentification(identification))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(identificationActions.setIdentificationUpdateMode(updateMode))
    }

    const editIdentification = async (identification: IIdentification) => {
        return await identificationService
            .update(identification)
            .then((identificationResponse) => {
                dispatch(identificationActions.editIdentificationSuccess(identificationResponse.data))
                setIdentification(identificationResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveIdentification = async (identification: IIdentification, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addIdentification(identification)
            : await editIdentification(identification)
    }

    useEffect(() => {
        loadIdentifications()
    }, [identification, identifications, isLoading, initialFetch, loadIdentifications])

    return {
        identification,
        identifications,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addIdentification,
        editIdentification,
        saveIdentification,
        setIdentification,
    }
}

export { useIdentification }
