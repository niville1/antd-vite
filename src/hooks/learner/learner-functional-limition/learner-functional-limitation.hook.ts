import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as learnerFunctionalLimitationActions from '../../../redux/learner/learner-functional-limitation/learner-functional-limition.slice'
import { ILearnerFunctionalLimitation } from '../../../models/learner/learner-functional-limitation.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import learnerFunctionalLimitationService from '../../../services/learner/learner-functional-limitation/learner-functional-limitation.service'

const useLearnerFunctionalLimitation = () => {
    const learnerFunctionalLimitations = useSelector<rootState, ILearnerFunctionalLimitation[]>(
        (state) => state.learnerFunctionalLimitation.learnerFunctionalLimitations
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.learnerFunctionalLimitation.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.learnerFunctionalLimitation.initialFetch
    )
    const learnerFunctionalLimitation = useSelector<rootState, ILearnerFunctionalLimitation>((state) => state.learnerFunctionalLimitation.learnerFunctionalLimitation)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.learnerFunctionalLimitation.updateMode
    )

    const dispatch = useDispatch()

    const loadLearnerFunctionalLimitations = useCallback(() => {
        if (initialFetch) {
            dispatch(learnerFunctionalLimitationActions.fetchLearnerFunctionalLimitationsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addLearnerFunctionalLimitation = async (learnerFunctionalLimitation: ILearnerFunctionalLimitation) => {
        return await learnerFunctionalLimitationService
            .create(learnerFunctionalLimitation)
            .then((learnerFunctionalLimitationResponse) => {
                if (learnerFunctionalLimitationResponse.success) {
                    dispatch(learnerFunctionalLimitationActions.addLearnerFunctionalLimitationSuccess(learnerFunctionalLimitationResponse.data))
                } else {
                    return learnerFunctionalLimitationResponse
                }
                return learnerFunctionalLimitationResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setLearnerFunctionalLimitation = (learnerFunctionalLimitation: ILearnerFunctionalLimitation) => {
        dispatch(learnerFunctionalLimitationActions.setActiveLearnerFunctionalLimitation(learnerFunctionalLimitation))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(learnerFunctionalLimitationActions.setLearnerFunctionalLimitationUpdateMode(updateMode))
    }

    const editLearnerFunctionalLimitation = async (learnerFunctionalLimitation: ILearnerFunctionalLimitation) => {
        return await learnerFunctionalLimitationService
            .update(learnerFunctionalLimitation)
            .then((learnerFunctionalLimitationResponse) => {
                dispatch(learnerFunctionalLimitationActions.editLearnerFunctionalLimitationSuccess(learnerFunctionalLimitationResponse.data))
                setLearnerFunctionalLimitation(learnerFunctionalLimitationResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveLearnerFunctionalLimitation = async (learnerFunctionalLimitation: ILearnerFunctionalLimitation, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addLearnerFunctionalLimitation(learnerFunctionalLimitation)
            : await editLearnerFunctionalLimitation(learnerFunctionalLimitation)
    }

    useEffect(() => {
        loadLearnerFunctionalLimitations()
    }, [learnerFunctionalLimitation, learnerFunctionalLimitations, isLoading, initialFetch, loadLearnerFunctionalLimitations])

    return {
        learnerFunctionalLimitation,
        learnerFunctionalLimitations,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addLearnerFunctionalLimitation,
        editLearnerFunctionalLimitation,
        saveLearnerFunctionalLimitation,
        setLearnerFunctionalLimitation,
    }
}

export { useLearnerFunctionalLimitation }
