import { difficultyLevelReducer } from "../../redux/learner/difficulty-level/difficulty-level.slice"
import { functionalLimitationGroupReducer } from "../../redux/learner/functional-limitation-group/functional-limition-group.slice"
import { functionalLimitationTypeReducer } from "../../redux/learner/functional-limitation-type/functional-limition-type.slice"
import { functionalLimitationReducer } from "../../redux/learner/functional-limitation/functional-limition.slice"
import { funderReducer } from "../../redux/learner/funder/funder.slice"
import { identificationReducer } from "../../redux/learner/identification/identification.slice"
import { interventionTypeReducer } from "../../redux/learner/intervention-type/intervention-type.slice"
import { learnerFunctionalLimitationGroupReducer } from "../../redux/learner/learner-functional-limitation-group/learner-functional-limition-group.slice"
import { learnerFunctionalLimitationtypeReducer } from "../../redux/learner/learner-functional-limitation-type/learner-functional-limition-type.slice"
import { learnerFunctionalLimitationReducer } from "../../redux/learner/learner-functional-limitation/learner-functional-limition.slice"
import { learnerinterventiontypeReducer } from "../../redux/learner/learner-intervention-type/learner-intervention-type.slice"
import { medicalPersonnelReducer } from "../../redux/learner/medical-personnel/medical-personnel.slice"
import { religionReducer } from "../../redux/learner/religion/religion.slice"
import { screeningResponseReducer } from "../../redux/learner/screening-response/screening-response.slice"



const reducers = {
    difficultyLevel: difficultyLevelReducer,
    functionalLimitation: functionalLimitationReducer,
    functionalLimitationGroup: functionalLimitationGroupReducer,
    functionalLimitationType: functionalLimitationTypeReducer,
    funder: funderReducer,
    identification: identificationReducer,
    interventionType: interventionTypeReducer,
    learnerFunctionalLimitation: learnerFunctionalLimitationReducer,
    learnerFunctionalLimitationGroup: learnerFunctionalLimitationGroupReducer,
    learnerFunctionalLimitationType: learnerFunctionalLimitationtypeReducer,
    learnerInterventionType: learnerinterventiontypeReducer,
    medicalPersonnel: medicalPersonnelReducer,
    religion: religionReducer,
    screeningResponse: screeningResponseReducer,   
}

export { reducers as learnerReducers }
