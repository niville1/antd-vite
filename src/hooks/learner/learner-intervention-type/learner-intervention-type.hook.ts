import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as learnerInterventionTypeActions from '../../../redux/learner/learner-intervention-type/learner-intervention-type.slice'
import { ILearnerInterventionType } from '../../../models/learner/learner-intervention-type.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import learnerInterventionTypeService from '../../../services/learner/learner-intervention-type/learner-intervention-type.service'

const useLearnerInterventionType = () => {
    const learnerInterventionTypes = useSelector<rootState, ILearnerInterventionType[]>(
        (state) => state.learnerInterventionType.learnerInterventionTypes
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.learnerInterventionType.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.learnerInterventionType.initialFetch
    )
    const learnerInterventionType = useSelector<rootState, ILearnerInterventionType>((state) => state.learnerInterventionType.learnerInterventionType)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.learnerInterventionType.updateMode
    )

    const dispatch = useDispatch()

    const loadLearnerInterventionTypes = useCallback(() => {
        if (initialFetch) {
            dispatch(learnerInterventionTypeActions.fetchLearnerInterventionTypesAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addLearnerInterventionType = async (learnerInterventionType: ILearnerInterventionType) => {
        return await learnerInterventionTypeService
            .create(learnerInterventionType)
            .then((learnerInterventionTypeResponse) => {
                if (learnerInterventionTypeResponse.success) {
                    dispatch(learnerInterventionTypeActions.addLearnerInterventionTypeSuccess(learnerInterventionTypeResponse.data))
                } else {
                    return learnerInterventionTypeResponse
                }
                return learnerInterventionTypeResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setLearnerInterventionType = (learnerInterventionType: ILearnerInterventionType) => {
        dispatch(learnerInterventionTypeActions.setActiveLearnerInterventionType(learnerInterventionType))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(learnerInterventionTypeActions.setLearnerInterventionTypeUpdateMode(updateMode))
    }

    const editLearnerInterventionType = async (learnerInterventionType: ILearnerInterventionType) => {
        return await learnerInterventionTypeService
            .update(learnerInterventionType)
            .then((learnerInterventionTypeResponse) => {
                dispatch(learnerInterventionTypeActions.editLearnerInterventionTypeSuccess(learnerInterventionTypeResponse.data))
                setLearnerInterventionType(learnerInterventionTypeResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveLearnerInterventionType = async (learnerInterventionType: ILearnerInterventionType, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addLearnerInterventionType(learnerInterventionType)
            : await editLearnerInterventionType(learnerInterventionType)
    }

    useEffect(() => {
        loadLearnerInterventionTypes()
    }, [learnerInterventionType, learnerInterventionTypes, isLoading, initialFetch, loadLearnerInterventionTypes])

    return {
        learnerInterventionType,
        learnerInterventionTypes,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addLearnerInterventionType,
        editLearnerInterventionType,
        saveLearnerInterventionType,
        setLearnerInterventionType,
    }
}

export { useLearnerInterventionType }
