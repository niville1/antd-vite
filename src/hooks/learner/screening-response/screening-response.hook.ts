import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as screeningResponseActions from '../../../redux/learner/screening-response/screening-response.slice'
import { IScreeningResponse } from '../../../models/learner/screening-response.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import screeningResponseService from '../../../services/learner/screening-response/screening-response.service'

const useScreeningResponse = () => {
    const screeningResponses = useSelector<rootState, IScreeningResponse[]>(
        (state) => state.screeningResponse.screeningResponses
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.screeningResponse.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.screeningResponse.initialFetch
    )
    const screeningResponse = useSelector<rootState, IScreeningResponse>((state) => state.screeningResponse.screeningResponse)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.screeningResponse.updateMode
    )

    const dispatch = useDispatch()

    const loadScreeningResponses = useCallback(() => {
        if (initialFetch) {
            dispatch(screeningResponseActions.fetchScreeningResponsesAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addScreeningResponse = async (screeningResponse: IScreeningResponse) => {
        return await screeningResponseService
            .create(screeningResponse)
            .then((screeningResponseResponse) => {
                if (screeningResponseResponse.success) {
                    dispatch(screeningResponseActions.addScreeningResponseSuccess(screeningResponseResponse.data))
                } else {
                    return screeningResponseResponse
                }
                return screeningResponseResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setScreeningResponse = (screeningResponse: IScreeningResponse) => {
        dispatch(screeningResponseActions.setActiveScreeningResponse(screeningResponse))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(screeningResponseActions.setScreeningResponseUpdateMode(updateMode))
    }

    const editScreeningResponse = async (screeningResponse: IScreeningResponse) => {
        return await screeningResponseService
            .update(screeningResponse)
            .then((screeningResponseResponse) => {
                dispatch(screeningResponseActions.editScreeningResponseSuccess(screeningResponseResponse.data))
                setScreeningResponse(screeningResponseResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveScreeningResponse = async (screeningResponse: IScreeningResponse, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addScreeningResponse(screeningResponse)
            : await editScreeningResponse(screeningResponse)
    }

    useEffect(() => {
        loadScreeningResponses()
    }, [screeningResponse, screeningResponses, isLoading, initialFetch, loadScreeningResponses])

    return {
        screeningResponse,
        screeningResponses,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addScreeningResponse,
        editScreeningResponse,
        saveScreeningResponse,
        setScreeningResponse,
    }
}

export { useScreeningResponse }
