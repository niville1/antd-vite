import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as learnerFunctionalLimitationGroupActions from '../../../redux/learner/learner-functional-limitation-group/learner-functional-limition-group.slice'
import { ILearnerFunctionalLimitationGroup } from '../../../models/learner/learner-functional-limitation-group.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import learnerFunctionalLimitationGroupService from '../../../services/learner/learner-functional-limitation-group/learner-functional-limitation-group.service'

const useLearnerFunctionalLimitationGroup = () => {
    const learnerFunctionalLimitationGroups = useSelector<rootState, ILearnerFunctionalLimitationGroup[]>(
        (state) => state.learnerFunctionalLimitationGroup.learnerFunctionalLimitationGroups
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.learnerFunctionalLimitationGroup.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.learnerFunctionalLimitationGroup.initialFetch
    )
    const learnerFunctionalLimitationGroup = useSelector<rootState, ILearnerFunctionalLimitationGroup>((state) => state.learnerFunctionalLimitationGroup.learnerFunctionalLimitationGroup)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.learnerFunctionalLimitationGroup.updateMode
    )

    const dispatch = useDispatch()

    const loadLearnerFunctionalLimitationGroups = useCallback(() => {
        if (initialFetch) {
            dispatch(learnerFunctionalLimitationGroupActions.fetchLearnerFunctionalLimitationGroupsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addLearnerFunctionalLimitationGroup = async (learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup) => {
        return await learnerFunctionalLimitationGroupService
            .create(learnerFunctionalLimitationGroup)
            .then((learnerFunctionalLimitationGroupResponse) => {
                if (learnerFunctionalLimitationGroupResponse.success) {
                    dispatch(learnerFunctionalLimitationGroupActions.addLearnerFunctionalLimitationGroupSuccess(learnerFunctionalLimitationGroupResponse.data))
                } else {
                    return learnerFunctionalLimitationGroupResponse
                }
                return learnerFunctionalLimitationGroupResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setLearnerFunctionalLimitationGroup = (learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup) => {
        dispatch(learnerFunctionalLimitationGroupActions.setActiveLearnerFunctionalLimitationGroup(learnerFunctionalLimitationGroup))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(learnerFunctionalLimitationGroupActions.setLearnerFunctionalLimitationGroupUpdateMode(updateMode))
    }

    const editLearnerFunctionalLimitationGroup = async (learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup) => {
        return await learnerFunctionalLimitationGroupService
            .update(learnerFunctionalLimitationGroup)
            .then((learnerFunctionalLimitationGroupResponse) => {
                dispatch(learnerFunctionalLimitationGroupActions.editLearnerFunctionalLimitationGroupSuccess(learnerFunctionalLimitationGroupResponse.data))
                setLearnerFunctionalLimitationGroup(learnerFunctionalLimitationGroupResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveLearnerFunctionalLimitationGroup = async (learnerFunctionalLimitationGroup: ILearnerFunctionalLimitationGroup, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addLearnerFunctionalLimitationGroup(learnerFunctionalLimitationGroup)
            : await editLearnerFunctionalLimitationGroup(learnerFunctionalLimitationGroup)
    }

    useEffect(() => {
        loadLearnerFunctionalLimitationGroups()
    }, [learnerFunctionalLimitationGroup, learnerFunctionalLimitationGroups, isLoading, initialFetch, loadLearnerFunctionalLimitationGroups])

    return {
        learnerFunctionalLimitationGroup,
        learnerFunctionalLimitationGroups,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addLearnerFunctionalLimitationGroup,
        editLearnerFunctionalLimitationGroup,
        saveLearnerFunctionalLimitationGroup,
        setLearnerFunctionalLimitationGroup,
    }
}

export { useLearnerFunctionalLimitationGroup }
