import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as functionalLimitationGroupActions from '../../../redux/learner/functional-limitation-group/functional-limition-group.slice'
import { IFunctionalLimitationGroup } from '../../../models/learner/functional-limitation-group.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import functionalLimitationGroupService from '../../../services/learner/functional-limitation-group/functional-limitation-group.service'

const useFunctionalLimitationGroup = () => {
    const functionalLimitationGroups = useSelector<rootState, IFunctionalLimitationGroup[]>(
        (state) => state.functionalLimitationGroup.functionalLimitationGroups
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.functionalLimitationGroup.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.functionalLimitationGroup.initialFetch
    )
    const functionalLimitationGroup = useSelector<rootState, IFunctionalLimitationGroup>((state) => state.functionalLimitationGroup.functionalLimitationGroup)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.functionalLimitationGroup.updateMode
    )

    const dispatch = useDispatch()

    const loadFunctionalLimitationGroups = useCallback(() => {
        if (initialFetch) {
            dispatch(functionalLimitationGroupActions.fetchFunctionalLimitationGroupsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addFunctionalLimitationGroup = async (functionalLimitationGroup: IFunctionalLimitationGroup) => {
        return await functionalLimitationGroupService
            .create(functionalLimitationGroup)
            .then((functionalLimitationGroupResponse) => {
                if (functionalLimitationGroupResponse.success) {
                    dispatch(functionalLimitationGroupActions.addFunctionalLimitationGroupSuccess(functionalLimitationGroupResponse.data))
                } else {
                    return functionalLimitationGroupResponse
                }
                return functionalLimitationGroupResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setFunctionalLimitationGroup = (functionalLimitationGroup: IFunctionalLimitationGroup) => {
        dispatch(functionalLimitationGroupActions.setActiveFunctionalLimitationGroup(functionalLimitationGroup))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(functionalLimitationGroupActions.setFunctionalLimitationGroupUpdateMode(updateMode))
    }

    const editFunctionalLimitationGroup = async (functionalLimitationGroup: IFunctionalLimitationGroup) => {
        return await functionalLimitationGroupService
            .update(functionalLimitationGroup)
            .then((functionalLimitationGroupResponse) => {
                dispatch(functionalLimitationGroupActions.editFunctionalLimitationGroupSuccess(functionalLimitationGroupResponse.data))
                setFunctionalLimitationGroup(functionalLimitationGroupResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveFunctionalLimitationGroup = async (functionalLimitationGroup: IFunctionalLimitationGroup, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addFunctionalLimitationGroup(functionalLimitationGroup)
            : await editFunctionalLimitationGroup(functionalLimitationGroup)
    }

    useEffect(() => {
        loadFunctionalLimitationGroups()
    }, [functionalLimitationGroup, functionalLimitationGroups, isLoading, initialFetch, loadFunctionalLimitationGroups])

    return {
        functionalLimitationGroup,
        functionalLimitationGroups,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addFunctionalLimitationGroup,
        editFunctionalLimitationGroup,
        saveFunctionalLimitationGroup,
        setFunctionalLimitationGroup,
    }
}

export { useFunctionalLimitationGroup }
