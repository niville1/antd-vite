import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as functionalLimitationActions from '../../../redux/learner/functional-limitation/functional-limition.slice'
import { IFunctionalLimitation } from '../../../models/learner/functional-limitation.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import functionalLimitationService from '../../../services/learner/functional-limitation/functional-limitation.service'

const useFunctionalLimitation = () => {
    const functionalLimitations = useSelector<rootState, IFunctionalLimitation[]>(
        (state) => state.functionalLimitation.functionalLimitations
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.functionalLimitation.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.functionalLimitation.initialFetch
    )
    const functionalLimitation = useSelector<rootState, IFunctionalLimitation>((state) => state.functionalLimitation.functionalLimitation)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.functionalLimitation.updateMode
    )

    const dispatch = useDispatch()

    const loadFunctionalLimitations = useCallback(() => {
        if (initialFetch) {
            dispatch(functionalLimitationActions.fetchFunctionalLimitationsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addFunctionalLimitation = async (functionalLimitation: IFunctionalLimitation) => {
        return await functionalLimitationService
            .create(functionalLimitation)
            .then((functionalLimitationResponse) => {
                if (functionalLimitationResponse.success) {
                    dispatch(functionalLimitationActions.addFunctionalLimitationSuccess(functionalLimitationResponse.data))
                } else {
                    return functionalLimitationResponse
                }
                return functionalLimitationResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setFunctionalLimitation = (functionalLimitation: IFunctionalLimitation) => {
        dispatch(functionalLimitationActions.setActiveFunctionalLimitation(functionalLimitation))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(functionalLimitationActions.setFunctionalLimitationUpdateMode(updateMode))
    }

    const editFunctionalLimitation = async (functionalLimitation: IFunctionalLimitation) => {
        return await functionalLimitationService
            .update(functionalLimitation)
            .then((functionalLimitationResponse) => {
                dispatch(functionalLimitationActions.editFunctionalLimitationSuccess(functionalLimitationResponse.data))
                setFunctionalLimitation(functionalLimitationResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveFunctionalLimitation = async (functionalLimitation: IFunctionalLimitation, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addFunctionalLimitation(functionalLimitation)
            : await editFunctionalLimitation(functionalLimitation)
    }

    useEffect(() => {
        loadFunctionalLimitations()
    }, [functionalLimitation, functionalLimitations, isLoading, initialFetch, loadFunctionalLimitations])

    return {
        functionalLimitation,
        functionalLimitations,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addFunctionalLimitation,
        editFunctionalLimitation,
        saveFunctionalLimitation,
        setFunctionalLimitation,
    }
}

export { useFunctionalLimitation }
