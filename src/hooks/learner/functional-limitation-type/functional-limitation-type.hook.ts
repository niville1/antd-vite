import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as functionalLimitationTypeActions from '../../../redux/learner/functional-limitation-type/functional-limition-type.slice'
import { IFunctionalLimitationType } from '../../../models/learner/functional-limitation-type.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import functionalLimitationTypeService from '../../../services/learner/functional-limitation-type/functional-limitation-type.service'

const useFunctionalLimitationType = () => {
    const functionalLimitationTypes = useSelector<rootState, IFunctionalLimitationType[]>(
        (state) => state.functionalLimitationType.functionalLimitationTypes
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.functionalLimitationType.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.functionalLimitationType.initialFetch
    )
    const functionalLimitationType = useSelector<rootState, IFunctionalLimitationType>((state) => state.functionalLimitationType.functionalLimitationType)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.functionalLimitationType.updateMode
    )

    const dispatch = useDispatch()

    const loadFunctionalLimitationTypes = useCallback(() => {
        if (initialFetch) {
            dispatch(functionalLimitationTypeActions.fetchFunctionalLimitationTypesAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addFunctionalLimitationType = async (functionalLimitationType: IFunctionalLimitationType) => {
        return await functionalLimitationTypeService
            .create(functionalLimitationType)
            .then((functionalLimitationTypeResponse) => {
                if (functionalLimitationTypeResponse.success) {
                    dispatch(functionalLimitationTypeActions.addFunctionalLimitationTypeSuccess(functionalLimitationTypeResponse.data))
                } else {
                    return functionalLimitationTypeResponse
                }
                return functionalLimitationTypeResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setFunctionalLimitationType = (functionalLimitationType: IFunctionalLimitationType) => {
        dispatch(functionalLimitationTypeActions.setActiveFunctionalLimitationType(functionalLimitationType))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(functionalLimitationTypeActions.setFunctionalLimitationTypeUpdateMode(updateMode))
    }

    const editFunctionalLimitationType = async (functionalLimitationType: IFunctionalLimitationType) => {
        return await functionalLimitationTypeService
            .update(functionalLimitationType)
            .then((functionalLimitationTypeResponse) => {
                dispatch(functionalLimitationTypeActions.editFunctionalLimitationTypeSuccess(functionalLimitationTypeResponse.data))
                setFunctionalLimitationType(functionalLimitationTypeResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveFunctionalLimitationType = async (functionalLimitationType: IFunctionalLimitationType, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addFunctionalLimitationType(functionalLimitationType)
            : await editFunctionalLimitationType(functionalLimitationType)
    }

    useEffect(() => {
        loadFunctionalLimitationTypes()
    }, [functionalLimitationType, functionalLimitationTypes, isLoading, initialFetch, loadFunctionalLimitationTypes])

    return {
        functionalLimitationType,
        functionalLimitationTypes,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addFunctionalLimitationType,
        editFunctionalLimitationType,
        saveFunctionalLimitationType,
        setFunctionalLimitationType,
    }
}

export { useFunctionalLimitationType }
