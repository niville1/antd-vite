import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import * as religionActions from '../../../redux/learner/religion/religion.slice'
import { IReligion } from '../../../models/learner/religion.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import religionService from '../../../services/learner/religion/religion.service'

const useReligion = () => {
    const religions = useSelector<rootState, IReligion[]>(
        (state) => state.religion.religions
    )
    const isLoading = useSelector<rootState, boolean>(
        (state) => state.religion.isLoading
    )
    const initialFetch = useSelector<rootState, boolean>(
        (state) => state.religion.initialFetch
    )
    const religion = useSelector<rootState, IReligion>((state) => state.religion.religion)
    const updateMode = useSelector<rootState, UpdateMode>(
        (state) => state.religion.updateMode
    )

    const dispatch = useDispatch()

    const loadReligions = useCallback(() => {
        if (initialFetch) {
            dispatch(religionActions.fetchReligionsAsync() as any)
        }
    }, [dispatch, initialFetch])

    const addReligion = async (religion: IReligion) => {
        return await religionService
            .create(religion)
            .then((religionResponse) => {
                if (religionResponse.success) {
                    dispatch(religionActions.addReligionSuccess(religionResponse.data))
                } else {
                    return religionResponse
                }
                return religionResponse.success
            })
            .catch((error) => {
                return error
            })
    }

    const setReligion = (religion: IReligion) => {
        dispatch(religionActions.setActiveReligion(religion))
    }

    const setUpdateMode = (updateMode: UpdateMode) => {
        dispatch(religionActions.setReligionUpdateMode(updateMode))
    }

    const editReligion = async (religion: IReligion) => {
        return await religionService
            .update(religion)
            .then((religionResponse) => {
                dispatch(religionActions.editReligionSuccess(religionResponse.data))
                setReligion(religionResponse.data)
                return true
            })
            .catch((error) => {
                return false
            })
    }

    const saveReligion = async (religion: IReligion, updateMode: UpdateMode) => {
        return updateMode === UpdateMode.ADD
            ? await addReligion(religion)
            : await editReligion(religion)
    }

    useEffect(() => {
        loadReligions()
    }, [religion, religions, isLoading, initialFetch, loadReligions])

    return {
        religion,
        religions,
        isLoading,
        initialFetch,
        updateMode,
        setUpdateMode,
        addReligion,
        editReligion,
        saveReligion,
        setReligion,
    }
}

export { useReligion }
