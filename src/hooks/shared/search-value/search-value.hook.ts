import { useCallback } from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { ISearchValue } from '../../../models/shared/search-value.model'
import { rootState } from '../../../redux/root-reducer'
import * as searchValueActions from '../../../redux/shared/search-value/search-value.actions'

const useSearchValue = () => {
    const dispatch = useDispatch()

    const searchValue = useSelector<rootState, ISearchValue>(
        (state) => state.searchValue.searchValue,
        shallowEqual
    )

    const setSearchValue = useCallback(
        (searchValue: string) => {
            dispatch(searchValueActions.setSearchValue(searchValue))
        },
        [dispatch]
    )

    return {
        searchValue,
        setSearchValue,
    }
}

export { useSearchValue }
