import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as tokenActions from "../../../redux/shared/token/token.actions";
import { rootState } from "../../../redux/root-reducer";
import { IToken } from "../../../models/shared/token.model";
import { tokenConfig } from "../../../models/config.model";

const useToken = () => {
  const dispatch = useDispatch();

  const token = useSelector<rootState, IToken>((state) => state.token.token);

  const isLoading = useSelector<rootState, boolean>(
    (state) => state.token.isLoading
  );

  useEffect(() => {
    (async () => {
      try {
        const accessToken = tokenConfig.token;
        dispatch(
          tokenActions.fetchTokensSuccess({
            accessToken: accessToken,
            email: "",
          })
        );
      } catch (e: any) {
        dispatch(tokenActions.fetchTokensError(e.message));
        console.error(e.message);
      }
    })();
  }, [dispatch]);

  return { loading: isLoading, token };
};

export { useToken };
