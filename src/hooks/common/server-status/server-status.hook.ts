import { rootState } from '../../../redux/root-reducer'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback, useEffect } from 'react'
import { useCurrentBranch } from '../current-branch/current-branch.hook'
import serverStatuseservice from '../../../services/common/server-status/server-status.service'
import { UpdateMode } from '../../../models/update-mode.enum'
import { IServerStatus, emptyServerStatus } from '../../../models/common/server-status.model'
import { addServerStatusSuccess, editServerStatusSuccess, fetchServerStatusesAsync, setActiveServerStatus, setServerStatusUpdateMode } from '../../../redux/common/server-status/server-status.slice'

const useServerStatus = () => {
	const { currentBranch } = useCurrentBranch()
	const serverStatuses = useSelector<rootState, IServerStatus[]>(
		(state) => state.serverStatus.serverStatuses
	)
	const isLoading = useSelector<rootState, boolean>(
		(state) => state.serverStatus.isLoading
	)
	const initialFetch = useSelector<rootState, boolean>(
		(state) => state.serverStatus.isLoading
	)
	const updateMode = useSelector<rootState, UpdateMode>(
		(state) => state.serverStatus.updateMode
	)

	const dispatch = useDispatch()

	const loadServerStatuses = useCallback(() => {
		if (isLoading) {
			return
		}
		if (initialFetch) {
			dispatch(fetchServerStatusesAsync() as any)
		}
	}, [dispatch, initialFetch, isLoading])

	const addServerStatus = async (serverStatus: IServerStatus) => {
		return await serverStatuseservice
		.create(serverStatus)
		.then((serverStatusResponse) => {
			dispatch(addServerStatusSuccess(serverStatusResponse.data))
			return true
		})
		.catch((error) => {
			return false
		})
	}

	const setServerStatus = (serverStatus: IServerStatus) => {
		dispatch(setActiveServerStatus(serverStatus))
	}

	const setUpdateMode = (updateMode: UpdateMode) => {
		dispatch(setServerStatusUpdateMode(updateMode))
	}

	const editServerStatus = async (serverStatus: IServerStatus) => {
		return await serverStatuseservice
		.update(serverStatus)
		.then((serverStatusResponse) => {
			dispatch(editServerStatusSuccess(serverStatusResponse.data))
			setServerStatus(serverStatusResponse.data)
			return true
		})
		.catch((error) => {
			return false
		})
	}

	const saveServerStatus = async (
		serverStatus: IServerStatus,
		updateMode: UpdateMode
	) => {
		return updateMode === UpdateMode.ADD
			? await addServerStatus(serverStatus)
			: await editServerStatus(serverStatus)
	}

	const getActiveBranchServerStatus = useCallback((): IServerStatus => {
		if(!serverStatuses){
			return emptyServerStatus;
		}
		const branchServerStatus = serverStatuses.find(
			(x) => x.branch === currentBranch.code
		)

		if (branchServerStatus) {
			return branchServerStatus
		}
		return emptyServerStatus
	}, [currentBranch, serverStatuses])

	const stateWorkSystem = useCallback((): boolean => {
		const serverStatus = getActiveBranchServerStatus()
		if (!serverStatus) {
			return false
		}
		return serverStatus.sysTrans === '50' && serverStatus.dayStatus === '01'
	}, [currentBranch, serverStatuses])

	const stateCashSystem = useCallback((): boolean => {
		const serverStatus = getActiveBranchServerStatus()
		if (!serverStatus) {
			return false
		}
		return serverStatus.cashTrans === '50'
	}, [currentBranch, serverStatuses])

	useEffect(() => {
		loadServerStatuses()
	}, [
		serverStatuses,
		isLoading,
		initialFetch,
		loadServerStatuses,
		getActiveBranchServerStatus,
	])

	return {
		serverStatus: getActiveBranchServerStatus(),
		stateWorkSystem,
		stateCashSystem,
		serverStatuses,
		isLoading,
		initialFetch,
		updateMode,
		setUpdateMode,
		addServerStatus,
		editServerStatus,
		saveServerStatus,
		setServerStatus,
	}
}

export { useServerStatus }
