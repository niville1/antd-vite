import { useDispatch, useSelector } from "react-redux";
import { useCallback } from "react";
import { rootState } from "../../../redux/root-reducer";
import { ISideBarToggle } from "../../../models/shared/side-bar-toggle.model";
import { toggleApplicationSideBar } from "../../../redux/common/side-bar-toggle/side-bar-toggle.actions";

const useSideBarToggle = () => {
  const sideBarToggle = useSelector<rootState, ISideBarToggle>(
    (state) => state.sideBarToggle.sideBarToggle
  );

  const dispatch = useDispatch();

  const toggleSideBar = useCallback(() => {
    dispatch(toggleApplicationSideBar(sideBarToggle));
  }, [dispatch, sideBarToggle]);

  return {
    collapsed: sideBarToggle.isOpen,
    toggleSideBar,
  };
};

export { useSideBarToggle };
