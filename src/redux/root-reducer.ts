import { Action, combineReducers } from "redux";
import { ThunkAction } from "redux-thunk";
import { commonReducers } from "./common";
import sessionStorage from "redux-persist/lib/storage/session";
import { persistReducer } from "redux-persist";
import { sharedReducers } from "./shared";
import { learnerReducers } from "./learner";

export const rootReducer = combineReducers({
  ...sharedReducers,
  ...commonReducers,
  ...learnerReducers,
});

export const persistConfig = {
  key: "root",
  storage: sessionStorage,
  whitelist: [
    "dialogMessage",
    "dateGeneration",
    "currentUser",
    "currentBranch",
    "branch",
    "serverStatus",
    "location",
    "segment",
    "activeTab",
    "user",
  ],
  blacklist: [],
};

export const persistedReducer = persistReducer(persistConfig, rootReducer);

export type rootState = ReturnType<typeof rootReducer>;

export type appThunk = ThunkAction<void, rootState, unknown, Action<string>>;
