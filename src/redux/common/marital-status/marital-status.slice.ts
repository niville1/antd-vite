import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import * as maritalStatusModel from '../../../models/common/marital-status.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import maritalStatusService from '../../../services/common/marital-status/marital-status.service'

export const initialState: maritalStatusModel.IMaritalStatusState = {
    maritalStatuses: [],
    errors: '',
    maritalStatus: maritalStatusModel.emptyMaritalStatus,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchMaritalStatusesAsync = createAsyncThunk(
    'maritalStatus/fetchMaritalStatusesAsync',
    async () => {
        return await maritalStatusService.list()
    }
)

export const maritalStatusSlice = createSlice({
    name: 'maritalStatus',
    initialState,
    reducers: {
        fetchMaritalStatusesRequest: (state) => {
            state.isLoading = true
        },
        fetchMaritalStatusesSuccess: (
            state,
            action: PayloadAction<maritalStatusModel.IMaritalStatus[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.maritalStatuses = action.payload
        },
        fetchMaritalStatusesError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editMaritalStatusSuccess: (
            state,
            action: PayloadAction<maritalStatusModel.IMaritalStatus>
        ) => {
            state.maritalStatuses = state.maritalStatuses.map((maritalStatus) => {
                return maritalStatus.code === action.payload.code
                    ? action.payload
                    : maritalStatus
            })
            state.updateMode = UpdateMode.NONE
        },
        addMaritalStatusSuccess: (
            state,
            action: PayloadAction<maritalStatusModel.IMaritalStatus>
        ) => {
            state.maritalStatuses = [...state.maritalStatuses, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveMaritalStatus: (
            state,
            action: PayloadAction<maritalStatusModel.IMaritalStatus>
        ) => {
            state.maritalStatus = action.payload
        },
        setMaritalStatusUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchMaritalStatusesAsync.pending, (_state, _action) => {
            // fetchMaritalStatusesRequest();
            _state.isLoading = true
        })
        builder.addCase(fetchMaritalStatusesAsync.fulfilled, (_state, action) => {
            // fetchMaritalStatusesSuccess(action.payload);
            _state.isLoading = false
            _state.initialFetch = false
            _state.maritalStatuses = action.payload
        })
        builder.addCase(fetchMaritalStatusesAsync.rejected, (_state, action) => {
            // fetchMaritalStatusesError(action.payload as string);
            _state.isLoading = false
            // state.errors = action.payload;
        })
    },
})

export const {
    fetchMaritalStatusesRequest,
    fetchMaritalStatusesSuccess,
    fetchMaritalStatusesError,
    editMaritalStatusSuccess,
    addMaritalStatusSuccess,
    setActiveMaritalStatus,
    setMaritalStatusUpdateMode,
} = maritalStatusSlice.actions

const reducer = maritalStatusSlice.reducer

export { reducer as maritalStatusReducer }
