import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import * as divisionModel from '../../../models/common/division.model'
import { UpdateMode } from '../../../models/update-mode.enum'
import divisionService from '../../../services/common/division/division.service'

export const initialState: divisionModel.IDivisionState = {
    divisions: [],
    errors: '',
    division: divisionModel.emptyDivision,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchDivisionsAsync = createAsyncThunk(
    'division/fetchDivisionsAsync',
    async () => {
        return await divisionService.list()
    }
)

export const divisionSlice = createSlice({
    name: 'division',
    initialState,
    reducers: {
        fetchDivisionsRequest: (state) => {
            state.isLoading = true
        },
        fetchDivisionsSuccess: (
            state,
            action: PayloadAction<divisionModel.IDivision[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.divisions = action.payload
        },
        fetchDivisionsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editDivisionSuccess: (
            state,
            action: PayloadAction<divisionModel.IDivision>
        ) => {
            state.divisions = state.divisions.map((division) => {
                return division.code === action.payload.code
                    ? action.payload
                    : division
            })
            state.updateMode = UpdateMode.NONE
        },
        addDivisionSuccess: (
            state,
            action: PayloadAction<divisionModel.IDivision>
        ) => {
            debugger
            state.divisions = [...state.divisions, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActivedivision: (
            state,
            action: PayloadAction<divisionModel.IDivision>
        ) => {
            state.division = action.payload
        },
        setDivisionUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchDivisionsAsync.pending, (_state, _action) => {
            // fetchdivisionsRequest();
            _state.isLoading = true
        })
        builder.addCase(fetchDivisionsAsync.fulfilled, (_state, action) => {
            // fetchdivisionsSuccess(action.payload);
            _state.isLoading = false
            _state.initialFetch = false
            _state.divisions = action.payload
        })
        builder.addCase(fetchDivisionsAsync.rejected, (_state, action) => {
            // fetchdivisionsError(action.payload as string);
            _state.isLoading = false
            // state.errors = action.payload;
        })
    },
})

export const {
    fetchDivisionsRequest,
    fetchDivisionsSuccess,
    fetchDivisionsError,
    editDivisionSuccess,
    addDivisionSuccess,
    setActivedivision,
    setDivisionUpdateMode,
} = divisionSlice.actions

const reducer = divisionSlice.reducer

export { reducer as divisionReducer }
