import { createSelector } from "reselect";
import { rootState } from "../../root-reducer";

export const getSideBarToggles = (state: rootState) => state.sideBarToggle;

export const getSideBarToggle = createSelector(
  [getSideBarToggles],
  (state) => state.sideBarToggle
);
