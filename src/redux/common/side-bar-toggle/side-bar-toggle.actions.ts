import { action } from "typesafe-actions";
import { ISideBarToggle } from "../../../models/shared/side-bar-toggle.model";
import {
  configurationActionsCreators,
  configurationActionTypes,
} from "./side-bar-toggle.types";

export const toggleApplicationSideBar = (
  sideBarToggle: ISideBarToggle
): configurationActionsCreators =>
  action(configurationActionTypes.SIDEBAR_TOGGLE, sideBarToggle);
