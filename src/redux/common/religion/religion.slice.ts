import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as religionModel from "../../../models/common/religion.model";
import { UpdateMode } from "../../../models/update-mode.enum";
import religionService from "../../../services/common/religion/religion.service";

export const initialState: religionModel.IReligionState = {
  religions: [],
  errors: "",
  religion: religionModel.emptyReligion,
  isLoading: false,
  initialFetch: true,
  updateMode: UpdateMode.NONE,
};

export const fetchReligionsAsync = createAsyncThunk(
  "religion/fetchReligionsAsync",
  async () => {
    return await religionService.list();
  }
);

export const religionSlice = createSlice({
  name: "religion",
  initialState,
  reducers: {
    fetchReligionsRequest: (state) => {
      state.isLoading = true;
    },
    fetchReligionsSuccess: (
      state,
      action: PayloadAction<religionModel.IReligion[]>
    ) => {
      state.isLoading = false;
      state.initialFetch = false;
      state.religions = action.payload;
    },
    fetchReligionsError: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.errors = action.payload;
    },
    editReligionSuccess: (state, action: PayloadAction<religionModel.IReligion>) => {
      state.religions = state.religions.map((religion) => {
        return religion.code === action.payload.code ? action.payload : religion;
      });
      state.updateMode = UpdateMode.NONE;
    },
    addReligionSuccess: (state, action: PayloadAction<religionModel.IReligion>) => {
      state.religions = [...state.religions, action.payload];
      state.updateMode = UpdateMode.NONE;
    },
    setActiveReligion: (state, action: PayloadAction<religionModel.IReligion>) => {
      state.religion = action.payload;
    },
    setReligionUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
      state.updateMode = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchReligionsAsync.pending, (_state, _action) => {
      // fetchReligionsRequest();
      _state.isLoading = true;
    });
    builder.addCase(fetchReligionsAsync.fulfilled, (_state, action) => {
      // fetchReligionsSuccess(action.payload);
      _state.isLoading = false;
      _state.initialFetch = false;
      _state.religions = action.payload;
    });
    builder.addCase(fetchReligionsAsync.rejected, (_state, action) => {
      // fetchReligionsError(action.payload as string);
      _state.isLoading = false;
      // state.errors = action.payload;
    });
  },
});

export const {
  fetchReligionsRequest,
  fetchReligionsSuccess,
  fetchReligionsError,
  editReligionSuccess,
  addReligionSuccess,
  setActiveReligion,
  setReligionUpdateMode,
} = religionSlice.actions;

const reducer = religionSlice.reducer;

export { reducer as religionReducer };
