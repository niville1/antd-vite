import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as learnerInterventionTypeModel from "../../../models/learner/learner-intervention-type.model"
import learnerInterventionTypeService from '../../../services/learner/learner-intervention-type/learner-intervention-type.service'

export const initialState: learnerInterventionTypeModel.ILearnerInterventionTypeState = {
    learnerInterventionTypes: [],
    errors: '',
    learnerInterventionType: learnerInterventionTypeModel.emptyLearnerInterventionType,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchLearnerInterventionTypesAsync = createAsyncThunk<learnerInterventionTypeModel.ILearnerInterventionType[], void>(
    'learnerInterventionType/fetchLearnerInterventionTypesAsync',
    async (_, thunkApi) => {
        try {
            return await learnerInterventionTypeService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const learnerInterventionTypeSlice = createSlice({
    name: 'learnerInterventionType',
    initialState,
    reducers: {
        fetchLearnerInterventionTypesRequest: (state) => {
            state.isLoading = true
        },
        fetchLearnerInterventionTypesSuccess: (
            state,
            action: PayloadAction<learnerInterventionTypeModel.ILearnerInterventionType[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerInterventionTypes = action.payload
        },
        fetchLearnerInterventionTypesError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editLearnerInterventionTypeSuccess: (state, action: PayloadAction<learnerInterventionTypeModel.ILearnerInterventionType>) => {
            state.learnerInterventionTypes = state.learnerInterventionTypes.map((learnerInterventionType) => {
                return learnerInterventionType.learner === action.payload.learner ? action.payload : learnerInterventionType
            })
            state.updateMode = UpdateMode.NONE
        },
        addLearnerInterventionTypeSuccess: (state, action: PayloadAction<learnerInterventionTypeModel.ILearnerInterventionType>) => {
            state.learnerInterventionTypes = [...state.learnerInterventionTypes, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveLearnerInterventionType: (state, action: PayloadAction<learnerInterventionTypeModel.ILearnerInterventionType>) => {
            state.learnerInterventionType = action.payload
        },
        setLearnerInterventionTypeUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchLearnerInterventionTypesAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchLearnerInterventionTypesAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerInterventionTypes = action.payload
        })
        builder.addCase(fetchLearnerInterventionTypesAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchLearnerInterventionTypesRequest,
    fetchLearnerInterventionTypesSuccess,
    fetchLearnerInterventionTypesError,
    editLearnerInterventionTypeSuccess,
    addLearnerInterventionTypeSuccess,
    setActiveLearnerInterventionType,
    setLearnerInterventionTypeUpdateMode,
} = learnerInterventionTypeSlice.actions

const reducer = learnerInterventionTypeSlice.reducer

export { reducer as learnerinterventiontypeReducer }
