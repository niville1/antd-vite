import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as religionModel from "../../../models/learner/religion.model"
import religionService from '../../../services/learner/religion/religion.service'

export const initialState: religionModel.IReligionState = {
    religions: [],
    errors: '',
    religion: religionModel.emptyReligion,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchReligionsAsync = createAsyncThunk<religionModel.IReligion[], void>(
    'religion/fetchReligionsAsync',
    async (_, thunkApi) => {
        try {
            return await religionService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const religionSlice = createSlice({
    name: 'religion',
    initialState,
    reducers: {
        fetchReligionsRequest: (state) => {
            state.isLoading = true
        },
        fetchReligionsSuccess: (
            state,
            action: PayloadAction<religionModel.IReligion[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.religions = action.payload
        },
        fetchReligionsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editReligionSuccess: (state, action: PayloadAction<religionModel.IReligion>) => {
            state.religions = state.religions.map((religion) => {
                return religion.code === action.payload.code ? action.payload : religion
            })
            state.updateMode = UpdateMode.NONE
        },
        addReligionSuccess: (state, action: PayloadAction<religionModel.IReligion>) => {
            state.religions = [...state.religions, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveReligion: (state, action: PayloadAction<religionModel.IReligion>) => {
            state.religion = action.payload
        },
        setReligionUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchReligionsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchReligionsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.religions = action.payload
        })
        builder.addCase(fetchReligionsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchReligionsRequest,
    fetchReligionsSuccess,
    fetchReligionsError,
    editReligionSuccess,
    addReligionSuccess,
    setActiveReligion,
    setReligionUpdateMode,
} = religionSlice.actions

const reducer = religionSlice.reducer

export { reducer as religionReducer }
