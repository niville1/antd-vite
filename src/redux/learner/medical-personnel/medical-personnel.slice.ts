import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as medicalPersonnelModel from "../../../models/learner/medical-personnel.model"
import medicalPersonnelService from '../../../services/learner/medical-personnel/medical-personnel.service'

export const initialState: medicalPersonnelModel.IMedicalPersonnelState = {
    medicalPersonnels: [],
    errors: '',
    medicalPersonnel: medicalPersonnelModel.emptyMedicalPersonnel,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchMedicalPersonnelsAsync = createAsyncThunk<medicalPersonnelModel.IMedicalPersonnel[], void>(
    'medicalPersonnel/fetchMedicalPersonnelsAsync',
    async (_, thunkApi) => {
        try {
            return await medicalPersonnelService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const medicalPersonnelSlice = createSlice({
    name: 'medicalPersonnel',
    initialState,
    reducers: {
        fetchMedicalPersonnelsRequest: (state) => {
            state.isLoading = true
        },
        fetchMedicalPersonnelsSuccess: (
            state,
            action: PayloadAction<medicalPersonnelModel.IMedicalPersonnel[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.medicalPersonnels = action.payload
        },
        fetchMedicalPersonnelsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editMedicalPersonnelSuccess: (state, action: PayloadAction<medicalPersonnelModel.IMedicalPersonnel>) => {
            state.medicalPersonnels = state.medicalPersonnels.map((medicalPersonnel) => {
                return medicalPersonnel.code === action.payload.code ? action.payload : medicalPersonnel
            })
            state.updateMode = UpdateMode.NONE
        },
        addMedicalPersonnelSuccess: (state, action: PayloadAction<medicalPersonnelModel.IMedicalPersonnel>) => {
            state.medicalPersonnels = [...state.medicalPersonnels, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveMedicalPersonnel: (state, action: PayloadAction<medicalPersonnelModel.IMedicalPersonnel>) => {
            state.medicalPersonnel = action.payload
        },
        setMedicalPersonnelUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchMedicalPersonnelsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchMedicalPersonnelsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.medicalPersonnels = action.payload
        })
        builder.addCase(fetchMedicalPersonnelsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchMedicalPersonnelsRequest,
    fetchMedicalPersonnelsSuccess,
    fetchMedicalPersonnelsError,
    editMedicalPersonnelSuccess,
    addMedicalPersonnelSuccess,
    setActiveMedicalPersonnel,
    setMedicalPersonnelUpdateMode,
} = medicalPersonnelSlice.actions

const reducer = medicalPersonnelSlice.reducer

export { reducer as medicalPersonnelReducer }
