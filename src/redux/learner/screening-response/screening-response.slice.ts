import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as screeningResponseModel from "../../../models/learner/screening-response.model"
import screeningResponseService from '../../../services/learner/screening-response/screening-response.service'

export const initialState: screeningResponseModel.IScreeningResponseState = {
    screeningResponses: [],
    errors: '',
    screeningResponse: screeningResponseModel.emptyScreeningResponse,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchScreeningResponsesAsync = createAsyncThunk<screeningResponseModel.IScreeningResponse[], void>(
    'screeningResponse/fetchScreeningResponsesAsync',
    async (_, thunkApi) => {
        try {
            return await screeningResponseService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const screeningResponseSlice = createSlice({
    name: 'screeningResponse',
    initialState,
    reducers: {
        fetchScreeningResponsesRequest: (state) => {
            state.isLoading = true
        },
        fetchScreeningResponsesSuccess: (
            state,
            action: PayloadAction<screeningResponseModel.IScreeningResponse[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.screeningResponses = action.payload
        },
        fetchScreeningResponsesError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editScreeningResponseSuccess: (state, action: PayloadAction<screeningResponseModel.IScreeningResponse>) => {
            state.screeningResponses = state.screeningResponses.map((screeningResponse) => {
                return screeningResponse.code === action.payload.code ? action.payload : screeningResponse
            })
            state.updateMode = UpdateMode.NONE
        },
        addScreeningResponseSuccess: (state, action: PayloadAction<screeningResponseModel.IScreeningResponse>) => {
            state.screeningResponses = [...state.screeningResponses, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveScreeningResponse: (state, action: PayloadAction<screeningResponseModel.IScreeningResponse>) => {
            state.screeningResponse = action.payload
        },
        setScreeningResponseUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchScreeningResponsesAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchScreeningResponsesAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.screeningResponses = action.payload
        })
        builder.addCase(fetchScreeningResponsesAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchScreeningResponsesRequest,
    fetchScreeningResponsesSuccess,
    fetchScreeningResponsesError,
    editScreeningResponseSuccess,
    addScreeningResponseSuccess,
    setActiveScreeningResponse,
    setScreeningResponseUpdateMode,
} = screeningResponseSlice.actions

const reducer = screeningResponseSlice.reducer

export { reducer as screeningResponseReducer }
