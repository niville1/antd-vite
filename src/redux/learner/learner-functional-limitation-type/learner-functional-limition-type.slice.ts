import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as learnerFunctionalLimitationTypeModel from "../../../models/learner/learner-functional-limitation-type.model"
import learnerFunctionalLimitationTypeService from '../../../services/learner/learner-functional-limitation-type/learner-functional-limitation-type.service'

export const initialState: learnerFunctionalLimitationTypeModel.ILearnerFunctionalLimitationTypeState = {
    learnerFunctionalLimitationTypes: [],
    errors: '',
    learnerFunctionalLimitationType: learnerFunctionalLimitationTypeModel.emptyLearnerFunctionalLimitationType,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchLearnerFunctionalLimitationTypesAsync = createAsyncThunk<learnerFunctionalLimitationTypeModel.ILearnerFunctionalLimitationType[], void>(
    'learnerFunctionalLimitationType/fetchLearnerFunctionalLimitationTypesAsync',
    async (_, thunkApi) => {
        try {
            return await learnerFunctionalLimitationTypeService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const learnerFunctionalLimitationTypeSlice = createSlice({
    name: 'learnerFunctionalLimitationType',
    initialState,
    reducers: {
        fetchLearnerFunctionalLimitationtypesRequest: (state) => {
            state.isLoading = true
        },
        fetchLearnerFunctionalLimitationTypesSuccess: (
            state,
            action: PayloadAction<learnerFunctionalLimitationTypeModel.ILearnerFunctionalLimitationType[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerFunctionalLimitationTypes = action.payload
        },
        fetchLearnerFunctionalLimitationTypesError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editLearnerFunctionalLimitationTypeSuccess: (state, action: PayloadAction<learnerFunctionalLimitationTypeModel.ILearnerFunctionalLimitationType>) => {
            state.learnerFunctionalLimitationTypes = state.learnerFunctionalLimitationTypes.map((learnerFunctionalLimitationType) => {
                return learnerFunctionalLimitationType.learner === action.payload.learner ? action.payload : learnerFunctionalLimitationType
            })
            state.updateMode = UpdateMode.NONE
        },
        addLearnerFunctionalLimitationTypeSuccess: (state, action: PayloadAction<learnerFunctionalLimitationTypeModel.ILearnerFunctionalLimitationType>) => {
            state.learnerFunctionalLimitationTypes = [...state.learnerFunctionalLimitationTypes, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveLearnerFunctionalLimitationType: (state, action: PayloadAction<learnerFunctionalLimitationTypeModel.ILearnerFunctionalLimitationType>) => {
            state.learnerFunctionalLimitationType = action.payload
        },
        setLearnerFunctionalLimitationTypeUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchLearnerFunctionalLimitationTypesAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchLearnerFunctionalLimitationTypesAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerFunctionalLimitationTypes = action.payload
        })
        builder.addCase(fetchLearnerFunctionalLimitationTypesAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchLearnerFunctionalLimitationtypesRequest,
    fetchLearnerFunctionalLimitationTypesSuccess,
    fetchLearnerFunctionalLimitationTypesError,
    editLearnerFunctionalLimitationTypeSuccess,
    addLearnerFunctionalLimitationTypeSuccess,
    setActiveLearnerFunctionalLimitationType,
    setLearnerFunctionalLimitationTypeUpdateMode,
} = learnerFunctionalLimitationTypeSlice.actions

const reducer = learnerFunctionalLimitationTypeSlice.reducer

export { reducer as learnerFunctionalLimitationtypeReducer }
