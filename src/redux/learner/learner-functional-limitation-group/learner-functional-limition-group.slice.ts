import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as learnerFunctionalLimitationGroupModel from "../../../models/learner/learner-functional-limitation-group.model"
import learnerFunctionalLimitationGroupService from '../../../services/learner/learner-functional-limitation-group/learner-functional-limitation-group.service'

export const initialState: learnerFunctionalLimitationGroupModel.ILearnerFunctionalLimitationGroupState = {
    learnerFunctionalLimitationGroups: [],
    errors: '',
    learnerFunctionalLimitationGroup: learnerFunctionalLimitationGroupModel.emptyLearnerFunctionalLimitationGroup,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchLearnerFunctionalLimitationGroupsAsync = createAsyncThunk<learnerFunctionalLimitationGroupModel.ILearnerFunctionalLimitationGroup[], void>(
    'learnerFunctionalLimitationGroup/fetchLearnerFunctionalLimitationGroupsAsync',
    async (_, thunkApi) => {
        try {
            return await learnerFunctionalLimitationGroupService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const learnerFunctionalLimitationGroupSlice = createSlice({
    name: 'learnerFunctionalLimitationGroup',
    initialState,
    reducers: {
        fetchLearnerFunctionalLimitationGroupsRequest: (state) => {
            state.isLoading = true
        },
        fetchLearnerFunctionalLimitationGroupsSuccess: (
            state,
            action: PayloadAction<learnerFunctionalLimitationGroupModel.ILearnerFunctionalLimitationGroup[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerFunctionalLimitationGroups = action.payload
        },
        fetchLearnerFunctionalLimitationGroupsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editLearnerFunctionalLimitationGroupSuccess: (state, action: PayloadAction<learnerFunctionalLimitationGroupModel.ILearnerFunctionalLimitationGroup>) => {
            state.learnerFunctionalLimitationGroups = state.learnerFunctionalLimitationGroups.map((learnerFunctionalLimitationGroup) => {
                return learnerFunctionalLimitationGroup.learner === action.payload.learner ? action.payload : learnerFunctionalLimitationGroup
            })
            state.updateMode = UpdateMode.NONE
        },
        addLearnerFunctionalLimitationGroupSuccess: (state, action: PayloadAction<learnerFunctionalLimitationGroupModel.ILearnerFunctionalLimitationGroup>) => {
            state.learnerFunctionalLimitationGroups = [...state.learnerFunctionalLimitationGroups, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveLearnerFunctionalLimitationGroup: (state, action: PayloadAction<learnerFunctionalLimitationGroupModel.ILearnerFunctionalLimitationGroup>) => {
            state.learnerFunctionalLimitationGroup = action.payload
        },
        setLearnerFunctionalLimitationGroupUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchLearnerFunctionalLimitationGroupsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchLearnerFunctionalLimitationGroupsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerFunctionalLimitationGroups = action.payload
        })
        builder.addCase(fetchLearnerFunctionalLimitationGroupsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchLearnerFunctionalLimitationGroupsRequest,
    fetchLearnerFunctionalLimitationGroupsSuccess,
    fetchLearnerFunctionalLimitationGroupsError,
    editLearnerFunctionalLimitationGroupSuccess,
    addLearnerFunctionalLimitationGroupSuccess,
    setActiveLearnerFunctionalLimitationGroup,
    setLearnerFunctionalLimitationGroupUpdateMode,
} = learnerFunctionalLimitationGroupSlice.actions

const reducer = learnerFunctionalLimitationGroupSlice.reducer

export { reducer as learnerFunctionalLimitationGroupReducer }
