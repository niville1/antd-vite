import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as functionalLimitationTypeModel from "../../../models/learner/functional-limitation-type.model"
import functionalLimitationTypeService from '../../../services/learner/functional-limitation-type/functional-limitation-type.service'

export const initialState: functionalLimitationTypeModel.IFunctionalLimitationTypeState = {
    functionalLimitationTypes: [],
    errors: '',
    functionalLimitationType: functionalLimitationTypeModel.emptyFunctionalLimitationType,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchFunctionalLimitationTypesAsync = createAsyncThunk<functionalLimitationTypeModel.IFunctionalLimitationType[], void>(
    'functionalLimitationType/fetchFunctionalLimitationTypesAsync',
    async (_, thunkApi) => {
        try {
            return await functionalLimitationTypeService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const functionalLimitationTypeSlice = createSlice({
    name: 'functionalLimitationType',
    initialState,
    reducers: {
        fetchFunctionalLimitationTypesRequest: (state) => {
            state.isLoading = true
        },
        fetchFunctionalLimitationTypesSuccess: (
            state,
            action: PayloadAction<functionalLimitationTypeModel.IFunctionalLimitationType[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.functionalLimitationTypes = action.payload
        },
        fetchFunctionalLimitationTypesError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editFunctionalLimitationTypeSuccess: (state, action: PayloadAction<functionalLimitationTypeModel.IFunctionalLimitationType>) => {
            state.functionalLimitationTypes = state.functionalLimitationTypes.map((functionalLimitationType) => {
                return functionalLimitationType.code === action.payload.code ? action.payload : functionalLimitationType
            })
            state.updateMode = UpdateMode.NONE
        },
        addFunctionalLimitationTypeSuccess: (state, action: PayloadAction<functionalLimitationTypeModel.IFunctionalLimitationType>) => {
            state.functionalLimitationTypes = [...state.functionalLimitationTypes, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveFunctionalLimitationType: (state, action: PayloadAction<functionalLimitationTypeModel.IFunctionalLimitationType>) => {
            state.functionalLimitationType = action.payload
        },
        setFunctionalLimitationTypeUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchFunctionalLimitationTypesAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchFunctionalLimitationTypesAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.functionalLimitationTypes = action.payload
        })
        builder.addCase(fetchFunctionalLimitationTypesAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchFunctionalLimitationTypesRequest,
    fetchFunctionalLimitationTypesSuccess,
    fetchFunctionalLimitationTypesError,
    editFunctionalLimitationTypeSuccess,
    addFunctionalLimitationTypeSuccess,
    setActiveFunctionalLimitationType,
    setFunctionalLimitationTypeUpdateMode,
} = functionalLimitationTypeSlice.actions

const reducer = functionalLimitationTypeSlice.reducer

export { reducer as functionalLimitationTypeReducer }
