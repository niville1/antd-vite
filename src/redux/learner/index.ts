import { difficultyLevelReducer } from "./difficulty-level/difficulty-level.slice"
import { functionalLimitationGroupReducer } from "./functional-limitation-group/functional-limition-group.slice"
import { functionalLimitationTypeReducer } from "./functional-limitation-type/functional-limition-type.slice"
import { functionalLimitationReducer } from "./functional-limitation/functional-limition.slice"
import { funderReducer } from "./funder/funder.slice"
import { identificationReducer } from "./identification/identification.slice"
import { interventionTypeReducer } from "./intervention-type/intervention-type.slice"
import { learnerFunctionalLimitationGroupReducer } from "./learner-functional-limitation-group/learner-functional-limition-group.slice"
import { learnerFunctionalLimitationtypeReducer } from "./learner-functional-limitation-type/learner-functional-limition-type.slice"
import { learnerFunctionalLimitationReducer } from "./learner-functional-limitation/learner-functional-limition.slice"
import { learnerinterventiontypeReducer } from "./learner-intervention-type/learner-intervention-type.slice"
import { medicalPersonnelReducer } from "./medical-personnel/medical-personnel.slice"
import { religionReducer } from "./religion/religion.slice"
import { screeningResponseReducer } from "./screening-response/screening-response.slice"


const reducers = {
    difficultyLevel: difficultyLevelReducer,
    functionalLimitation: functionalLimitationReducer,
    functionalLimitationGroup: functionalLimitationGroupReducer,
    functionalLimitationType: functionalLimitationTypeReducer,
    funder: funderReducer,
    identification: identificationReducer,
    interventionType: interventionTypeReducer,
    learnerFunctionalLimitation: learnerFunctionalLimitationReducer,
    learnerFunctionalLimitationGroup: learnerFunctionalLimitationGroupReducer,
    learnerFunctionalLimitationType: learnerFunctionalLimitationtypeReducer,
    learnerInterventionType: learnerinterventiontypeReducer,
    medicalPersonnel: medicalPersonnelReducer,
    religion: religionReducer,
    screeningResponse: screeningResponseReducer,   
}

export { reducers as learnerReducers }
