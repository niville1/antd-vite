import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as functionalLimitationGroupModel from "../../../models/learner/functional-limitation-group.model"
import functionalLimitationGroupService from '../../../services/learner/functional-limitation-group/functional-limitation-group.service'

export const initialState: functionalLimitationGroupModel.IFunctionalLimitationGroupState = {
    functionalLimitationGroups: [],
    errors: '',
    functionalLimitationGroup: functionalLimitationGroupModel.emptyFunctionalLimitationGroup,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchFunctionalLimitationGroupsAsync = createAsyncThunk<functionalLimitationGroupModel.IFunctionalLimitationGroup[], void>(
    'functionalLimitationGroup/fetchFunctionalLimitationGroupsAsync',
    async (_, thunkApi) => {
        try {
            return await functionalLimitationGroupService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const functionalLimitationGroupSlice = createSlice({
    name: 'functionalLimitationGroup',
    initialState,
    reducers: {
        fetchFunctionalLimitationGroupsRequest: (state) => {
            state.isLoading = true
        },
        fetchFunctionalLimitationGroupsSuccess: (
            state,
            action: PayloadAction<functionalLimitationGroupModel.IFunctionalLimitationGroup[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.functionalLimitationGroups = action.payload
        },
        fetchFunctionalLimitationGroupsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editFunctionalLimitationGroupSuccess: (state, action: PayloadAction<functionalLimitationGroupModel.IFunctionalLimitationGroup>) => {
            state.functionalLimitationGroups = state.functionalLimitationGroups.map((functionalLimitationGroup) => {
                return functionalLimitationGroup.code === action.payload.code ? action.payload : functionalLimitationGroup
            })
            state.updateMode = UpdateMode.NONE
        },
        addFunctionalLimitationGroupSuccess: (state, action: PayloadAction<functionalLimitationGroupModel.IFunctionalLimitationGroup>) => {
            state.functionalLimitationGroups = [...state.functionalLimitationGroups, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveFunctionalLimitationGroup: (state, action: PayloadAction<functionalLimitationGroupModel.IFunctionalLimitationGroup>) => {
            state.functionalLimitationGroup = action.payload
        },
        setFunctionalLimitationGroupUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchFunctionalLimitationGroupsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchFunctionalLimitationGroupsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.functionalLimitationGroups = action.payload
        })
        builder.addCase(fetchFunctionalLimitationGroupsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchFunctionalLimitationGroupsRequest,
    fetchFunctionalLimitationGroupsSuccess,
    fetchFunctionalLimitationGroupsError,
    editFunctionalLimitationGroupSuccess,
    addFunctionalLimitationGroupSuccess,
    setActiveFunctionalLimitationGroup,
    setFunctionalLimitationGroupUpdateMode,
} = functionalLimitationGroupSlice.actions

const reducer = functionalLimitationGroupSlice.reducer

export { reducer as functionalLimitationGroupReducer }
