import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as learnerFunctionalLimitationModel from "../../../models/learner/learner-functional-limitation.model"
import learnerFunctionalLimitationService from '../../../services/learner/learner-functional-limitation/learner-functional-limitation.service'

export const initialState: learnerFunctionalLimitationModel.ILearnerFunctionalLimitationState = {
    learnerFunctionalLimitations: [],
    errors: '',
    learnerFunctionalLimitation: learnerFunctionalLimitationModel.emptyLearnerFunctionalLimitation,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchLearnerFunctionalLimitationsAsync = createAsyncThunk<learnerFunctionalLimitationModel.ILearnerFunctionalLimitation[], void>(
    'learnerFunctionalLimitation/fetchLearnerFunctionalLimitationsAsync',
    async (_, thunkApi) => {
        try {
            return await learnerFunctionalLimitationService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const learnerFunctionalLimitationSlice = createSlice({
    name: 'learnerFunctionalLimitation',
    initialState,
    reducers: {
        fetchLearnerFunctionalLimitationsRequest: (state) => {
            state.isLoading = true
        },
        fetchLearnerFunctionalLimitationsSuccess: (
            state,
            action: PayloadAction<learnerFunctionalLimitationModel.ILearnerFunctionalLimitation[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerFunctionalLimitations = action.payload
        },
        fetchLearnerFunctionalLimitationsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editLearnerFunctionalLimitationSuccess: (state, action: PayloadAction<learnerFunctionalLimitationModel.ILearnerFunctionalLimitation>) => {
            state.learnerFunctionalLimitations = state.learnerFunctionalLimitations.map((learnerFunctionalLimitation) => {
                return learnerFunctionalLimitation.learner === action.payload.learner ? action.payload : learnerFunctionalLimitation
            })
            state.updateMode = UpdateMode.NONE
        },
        addLearnerFunctionalLimitationSuccess: (state, action: PayloadAction<learnerFunctionalLimitationModel.ILearnerFunctionalLimitation>) => {
            state.learnerFunctionalLimitations = [...state.learnerFunctionalLimitations, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveLearnerFunctionalLimitation: (state, action: PayloadAction<learnerFunctionalLimitationModel.ILearnerFunctionalLimitation>) => {
            state.learnerFunctionalLimitation = action.payload
        },
        setLearnerFunctionalLimitationUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchLearnerFunctionalLimitationsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchLearnerFunctionalLimitationsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.learnerFunctionalLimitations = action.payload
        })
        builder.addCase(fetchLearnerFunctionalLimitationsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchLearnerFunctionalLimitationsRequest,
    fetchLearnerFunctionalLimitationsSuccess,
    fetchLearnerFunctionalLimitationsError,
    editLearnerFunctionalLimitationSuccess,
    addLearnerFunctionalLimitationSuccess,
    setActiveLearnerFunctionalLimitation,
    setLearnerFunctionalLimitationUpdateMode,
} = learnerFunctionalLimitationSlice.actions

const reducer = learnerFunctionalLimitationSlice.reducer

export { reducer as learnerFunctionalLimitationReducer }
