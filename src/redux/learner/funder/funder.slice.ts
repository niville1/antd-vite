import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as funderModel from "../../../models/learner/funder.model"
import funderService from '../../../services/learner/funder/funder.service'

export const initialState: funderModel.IFunderState = {
    funders: [],
    errors: '',
    funder: funderModel.emptyFunder,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchFundersAsync = createAsyncThunk<funderModel.IFunder[], void>(
    'funder/fetchFundersAsync',
    async (_, thunkApi) => {
        try {
            return await funderService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const funderSlice = createSlice({
    name: 'funder',
    initialState,
    reducers: {
        fetchFundersRequest: (state) => {
            state.isLoading = true
        },
        fetchFundersSuccess: (
            state,
            action: PayloadAction<funderModel.IFunder[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.funders = action.payload
        },
        fetchFundersError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editFunderSuccess: (state, action: PayloadAction<funderModel.IFunder>) => {
            state.funders = state.funders.map((funder) => {
                return funder.code === action.payload.code ? action.payload : funder
            })
            state.updateMode = UpdateMode.NONE
        },
        addFunderSuccess: (state, action: PayloadAction<funderModel.IFunder>) => {
            state.funders = [...state.funders, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveFunder: (state, action: PayloadAction<funderModel.IFunder>) => {
            state.funder = action.payload
        },
        setFunderUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchFundersAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchFundersAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.funders = action.payload
        })
        builder.addCase(fetchFundersAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchFundersRequest,
    fetchFundersSuccess,
    fetchFundersError,
    editFunderSuccess,
    addFunderSuccess,
    setActiveFunder,
    setFunderUpdateMode,
} = funderSlice.actions

const reducer = funderSlice.reducer

export { reducer as funderReducer }
