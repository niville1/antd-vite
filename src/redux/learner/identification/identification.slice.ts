import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as identificationModel from "../../../models/learner/identification.model"
import identificationService from '../../../services/learner/identification/identification.service'

export const initialState: identificationModel.IIdentificationState = {
    identifications: [],
    errors: '',
    identification: identificationModel.emptyIdentification,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchIdentificationsAsync = createAsyncThunk<identificationModel.IIdentification[], void>(
    'identification/fetchIdentificationsAsync',
    async (_, thunkApi) => {
        try {
            return await identificationService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const identificationSlice = createSlice({
    name: 'identification',
    initialState,
    reducers: {
        fetchIdentificationsRequest: (state) => {
            state.isLoading = true
        },
        fetchIdentificationsSuccess: (
            state,
            action: PayloadAction<identificationModel.IIdentification[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.identifications = action.payload
        },
        fetchIdentificationsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editIdentificationSuccess: (state, action: PayloadAction<identificationModel.IIdentification>) => {
            state.identifications = state.identifications.map((identification) => {
                return identification.name === action.payload.name ? action.payload : identification
            })
            state.updateMode = UpdateMode.NONE
        },
        addIdentificationSuccess: (state, action: PayloadAction<identificationModel.IIdentification>) => {
            state.identifications = [...state.identifications, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveIdentification: (state, action: PayloadAction<identificationModel.IIdentification>) => {
            state.identification = action.payload
        },
        setIdentificationUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchIdentificationsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchIdentificationsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.identifications = action.payload
        })
        builder.addCase(fetchIdentificationsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchIdentificationsRequest,
    fetchIdentificationsSuccess,
    fetchIdentificationsError,
    editIdentificationSuccess,
    addIdentificationSuccess,
    setActiveIdentification,
    setIdentificationUpdateMode,
} = identificationSlice.actions

const reducer = identificationSlice.reducer

export { reducer as identificationReducer }
