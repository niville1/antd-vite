import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as interventionTypeModel from "../../../models/learner/intervention-type.model"
import interventionTypeService from '../../../services/learner/intervention-type/intervention-type.service'

export const initialState: interventionTypeModel.IInterventionTypeState = {
    interventionTypes: [],
    errors: '',
    interventionType: interventionTypeModel.emptyInterventionType,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchInterventionTypesAsync = createAsyncThunk<interventionTypeModel.IInterventionType[], void>(
    'interventionType/fetchInterventionTypesAsync',
    async (_, thunkApi) => {
        try {
            return await interventionTypeService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const interventionTypeSlice = createSlice({
    name: 'interventionType',
    initialState,
    reducers: {
        fetchInterventionTypesRequest: (state) => {
            state.isLoading = true
        },
        fetchInterventionTypesSuccess: (
            state,
            action: PayloadAction<interventionTypeModel.IInterventionType[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.interventionTypes = action.payload
        },
        fetchInterventionTypesError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editInterventionTypeSuccess: (state, action: PayloadAction<interventionTypeModel.IInterventionType>) => {
            state.interventionTypes = state.interventionTypes.map((interventionType) => {
                return interventionType.code === action.payload.code ? action.payload : interventionType
            })
            state.updateMode = UpdateMode.NONE
        },
        addInterventionTypeSuccess: (state, action: PayloadAction<interventionTypeModel.IInterventionType>) => {
            state.interventionTypes = [...state.interventionTypes, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveInterventionType: (state, action: PayloadAction<interventionTypeModel.IInterventionType>) => {
            state.interventionType = action.payload
        },
        setInterventionTypeUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchInterventionTypesAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchInterventionTypesAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.interventionTypes = action.payload
        })
        builder.addCase(fetchInterventionTypesAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchInterventionTypesRequest,
    fetchInterventionTypesSuccess,
    fetchInterventionTypesError,
    editInterventionTypeSuccess,
    addInterventionTypeSuccess,
    setActiveInterventionType,
    setInterventionTypeUpdateMode,
} = interventionTypeSlice.actions

const reducer = interventionTypeSlice.reducer

export { reducer as interventionTypeReducer }
