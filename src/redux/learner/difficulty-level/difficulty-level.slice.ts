import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as difficultyLevelModel from "../../../models/learner/difficulty-level.model"
import difficultyLevelService from '../../../services/learner/difficulty-level/difficulty-level.service'

export const initialState: difficultyLevelModel.IDifficultyLevelState = {
    difficultyLevels: [],
    errors: '',
    difficultyLevel: difficultyLevelModel.emptyDifficultyLevel,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchDifficultyLevelsAsync = createAsyncThunk<difficultyLevelModel.IDifficultyLevel[], void>(
    'difficultyLevel/fetchDifficultyLevelsAsync',
    async (_, thunkApi) => {
        try {
            return await difficultyLevelService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const difficultyLevelSlice = createSlice({
    name: 'difficultyLevel',
    initialState,
    reducers: {
        fetchDifficultyLevelsRequest: (state) => {
            state.isLoading = true
        },
        fetchDifficultyLevelsSuccess: (
            state,
            action: PayloadAction<difficultyLevelModel.IDifficultyLevel[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.difficultyLevels = action.payload
        },
        fetchDifficultyLevelsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editDifficultyLevelSuccess: (state, action: PayloadAction<difficultyLevelModel.IDifficultyLevel>) => {
            state.difficultyLevels = state.difficultyLevels.map((difficultyLevel) => {
                return difficultyLevel.code === action.payload.code ? action.payload : difficultyLevel
            })
            state.updateMode = UpdateMode.NONE
        },
        addDifficultyLevelSuccess: (state, action: PayloadAction<difficultyLevelModel.IDifficultyLevel>) => {
            state.difficultyLevels = [...state.difficultyLevels, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveDifficultyLevel: (state, action: PayloadAction<difficultyLevelModel.IDifficultyLevel>) => {
            state.difficultyLevel = action.payload
        },
        setDifficultyLevelUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchDifficultyLevelsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchDifficultyLevelsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.difficultyLevels = action.payload
        })
        builder.addCase(fetchDifficultyLevelsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchDifficultyLevelsRequest,
    fetchDifficultyLevelsSuccess,
    fetchDifficultyLevelsError,
    editDifficultyLevelSuccess,
    addDifficultyLevelSuccess,
    setActiveDifficultyLevel,
    setDifficultyLevelUpdateMode,
} = difficultyLevelSlice.actions

const reducer = difficultyLevelSlice.reducer

export { reducer as difficultyLevelReducer }
