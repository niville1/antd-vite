import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UpdateMode } from '../../../models/update-mode.enum'
import * as functionalLimitationModel from "../../../models/learner/functional-limitation.model"
import functionalLimitationService from '../../../services/learner/functional-limitation/functional-limitation.service'

export const initialState: functionalLimitationModel.IFunctionalLimitationState = {
    functionalLimitations: [],
    errors: '',
    functionalLimitation: functionalLimitationModel.emptyFunctionalLimitation,
    isLoading: false,
    initialFetch: true,
    updateMode: UpdateMode.NONE,
}

export const fetchFunctionalLimitationsAsync = createAsyncThunk<functionalLimitationModel.IFunctionalLimitation[], void>(
    'functionalLimitation/fetchFunctionalLimitationsAsync',
    async (_, thunkApi) => {
        try {
            return await functionalLimitationService.list()
        } catch (error: any) {
            return thunkApi.rejectWithValue({ error: error.data })
        }
    }
)

export const functionalLimitationSlice = createSlice({
    name: 'functionalLimitation',
    initialState,
    reducers: {
        fetchFunctionalLimitationsRequest: (state) => {
            state.isLoading = true
        },
        fetchFunctionalLimitationsSuccess: (
            state,
            action: PayloadAction<functionalLimitationModel.IFunctionalLimitation[]>
        ) => {
            state.isLoading = false
            state.initialFetch = false
            state.functionalLimitations = action.payload
        },
        fetchFunctionalLimitationsError: (state, action: PayloadAction<string>) => {
            state.isLoading = false
            state.errors = action.payload
        },
        editFunctionalLimitationSuccess: (state, action: PayloadAction<functionalLimitationModel.IFunctionalLimitation>) => {
            state.functionalLimitations = state.functionalLimitations.map((functionalLimitation) => {
                return functionalLimitation.code === action.payload.code ? action.payload : functionalLimitation
            })
            state.updateMode = UpdateMode.NONE
        },
        addFunctionalLimitationSuccess: (state, action: PayloadAction<functionalLimitationModel.IFunctionalLimitation>) => {
            state.functionalLimitations = [...state.functionalLimitations, action.payload]
            state.updateMode = UpdateMode.NONE
        },
        setActiveFunctionalLimitation: (state, action: PayloadAction<functionalLimitationModel.IFunctionalLimitation>) => {
            state.functionalLimitation = action.payload
        },
        setFunctionalLimitationUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
            state.updateMode = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchFunctionalLimitationsAsync.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchFunctionalLimitationsAsync.fulfilled, (state, action) => {
            state.isLoading = false
            state.initialFetch = false
            state.functionalLimitations = action.payload
        })
        builder.addCase(fetchFunctionalLimitationsAsync.rejected, (state, action) => {
            state.isLoading = false
            state.errors = action.payload
        })
    },
})

export const {
    fetchFunctionalLimitationsRequest,
    fetchFunctionalLimitationsSuccess,
    fetchFunctionalLimitationsError,
    editFunctionalLimitationSuccess,
    addFunctionalLimitationSuccess,
    setActiveFunctionalLimitation,
    setFunctionalLimitationUpdateMode,
} = functionalLimitationSlice.actions

const reducer = functionalLimitationSlice.reducer

export { reducer as functionalLimitationReducer }
