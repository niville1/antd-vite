import { action } from "typesafe-actions";

import { LanguageActionCreators, LanguageActionTypes } from "./lang.types";

export const changeLanguage = (languageKey: string): LanguageActionCreators => {
  return action(LanguageActionTypes.CHANGE, languageKey);
};
