import { createSelector } from "reselect";
import { rootState } from "../../root-reducer";

export const getLanguage = (state: rootState) => state.language;

export const getCurrentLanguage = createSelector(
  [getLanguage],
  (languageState) => languageState.key
);
