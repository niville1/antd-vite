import { action } from 'typesafe-actions'
import { activeTabActionsCreators, activeTabActionTypes } from './active-tab.types'

export const setActiveTabTab = (activeTab: string): activeTabActionsCreators =>
    action(activeTabActionTypes.SET_ACTIVE_TAB, activeTab)
