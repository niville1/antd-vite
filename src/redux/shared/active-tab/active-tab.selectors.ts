import { createSelector } from 'reselect'
import { rootState } from '../../root-reducer'

export const getActiveTabs = (state: rootState) => state.activeTab

export const getActiveActiveTabs = createSelector(
    [getActiveTabs],
    (state) => state.activeTab
)
