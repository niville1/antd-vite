import { IBranch } from '../../../models/common/branch.model'
import {
    emptyCurrentBranch,
    ICurrentBranch,
} from '../../../models/common/current-branch.model'
import { fetchBranchesAsync } from '../../common/branch/branch.slice'
import { fetchServerStatusesSuccess } from '../../common/server-status/server-status.slice'

import { fetchCurrentBranchSuccess } from '../../common/current-branch/current-branch.slice'
import { fetchCurrentUserSuccess } from '../../common/current-user/current-user.slice'
import { fetchDateGenerationsSuccess } from '../../common/date-generation/date-generation.slice'
import { appThunk } from '../../root-reducer'
import {
    fetchSessionBatchError,
    fetchSessionBatchRequest,
    fetchSessionBatchSuccess,
} from './session-batch.slice'
import { fetchBranchStationsAsync } from '../../common/branch-station/branch-station.slice'
import { fetchDialogMessagesAsync } from '../../common/dialog-message/dialog-message.slice'
import { sessionBatchService } from '../../../services/batch/session-batch.service'
import { ISessionBatch } from '../../../models/batch/session-batch.model'

const fetchRemainingInitialDataAsync = (): appThunk => async (dispatch) => {
    dispatch(fetchBranchStationsAsync())
    dispatch(fetchBranchesAsync())

    dispatch(fetchDialogMessagesAsync())
}

const getRegionCurrentBranch = (
    allBranches: IBranch[],
    activeBranchCode: string
): ICurrentBranch => {
    if (allBranches) {
        var branch = allBranches.find((b) => b.code === activeBranchCode)

        return branch as ICurrentBranch
    }
    return {
        ...emptyCurrentBranch,
        code: activeBranchCode,
    }
}

export const fetchInitialDataAsync = (): appThunk => async (dispatch) => {
    try {
        dispatch(fetchSessionBatchRequest())

        sessionBatchService.getBatch().then((batch: ISessionBatch) => {
            const currentBranch = getRegionCurrentBranch(
                batch.branches,
                batch.branch
            )

            dispatch(fetchCurrentBranchSuccess(currentBranch))
            dispatch(fetchCurrentUserSuccess(batch.currentUser))
            // dispatch(fetchBranchesSuccess(batch.branches))
            dispatch(fetchServerStatusesSuccess(batch.serverStatuses))
            dispatch(fetchDateGenerationsSuccess(batch.dateGenerations))

            dispatch(fetchSessionBatchSuccess())
            dispatch(fetchRemainingInitialDataAsync())
        })
    } catch (error: any) {
        dispatch(fetchSessionBatchError(error?.message))
    }
}

export const fetchInitialDataBranchAsync =
    (branch: string): appThunk =>
    async (dispatch) => {
        try {
            dispatch(fetchBranchesAsync())
        } catch (error) {
            //dispatch(currentBranchActions.fetchCurrentBranchesError(error.message));
        }
    }
