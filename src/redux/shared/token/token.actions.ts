import { action } from "typesafe-actions";
import { IToken } from "../../../models/shared/token.model";
import { TokenActionsCreators, TokenActionTypes } from "./token.types";

export const fetchTokensRequest = (): TokenActionsCreators => {
  return action(TokenActionTypes.TOKEN_FETCH_REQUEST);
};

export const fetchTokensSuccess = (token: IToken): TokenActionsCreators =>
  action(TokenActionTypes.TOKEN_FETCH_SUCCESS, token);

export const fetchTokensError = (message: string): TokenActionsCreators =>
  action(TokenActionTypes.TOKEN_FETCH_ERROR, message);
