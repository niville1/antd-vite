import { action } from 'typesafe-actions'
import { searchValueActionsCreators, searchValueActionTypes } from './search-value.types'

export const setSearchValue = (searchValue: any): searchValueActionsCreators =>
    action(searchValueActionTypes.SET_SEARCH_VALUE, searchValue)
