import { createSelector } from 'reselect'
import { rootState } from '../../root-reducer'

export const getSearchValues = (state: rootState) => state.searchValue

export const getActiveSearchValues = createSelector(
    [getSearchValues],
    (state) => state.searchValue
)
