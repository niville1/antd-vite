import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as userModel from "../../../models/shared/user.model";
import { UpdateMode } from "../../../models/update-mode.enum";
import userService from "../../../services/shared/user/user.service";

export const initialState: userModel.IUserState = {
  users: [],
  errors: "",
  user: userModel.emptyUser,
  isLoading: false,
  initialFetch: true,
  updateMode: UpdateMode.NONE,
};

export const fetchUsersAsync = createAsyncThunk(
  "user/fetchUsersAsync",
  async () => {
    return await userService.list();
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    fetchUsersRequest: (state) => {
      state.isLoading = true;
    },
    fetchUsersSuccess: (
      state,
      action: PayloadAction<userModel.IUser[]>
    ) => {
      state.isLoading = false;
      state.initialFetch = false;
      state.users = action.payload;
    },
    fetchUsersError: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.errors = action.payload;
    },
    searchUsersRequest:(state) => {
      state.isLoading = true;
    },
    searchUsersSuccess: (state, action: PayloadAction<userModel.IUser[]>) => {
      state.isLoading = false;
      state.initialFetch = false;
      state.users = action.payload;
    },
    searchUsersError: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.errors = action.payload;
    },
    editUserSuccess: (state, action: PayloadAction<userModel.IUser>) => {
      state.users = state.users.map((user) => {
        return user.userCode === action.payload.userCode ? action.payload : user;
      });
      state.updateMode = UpdateMode.NONE;
    },
    addUserSuccess: (state, action: PayloadAction<userModel.IUser>) => {
      state.users = [...state.users, action.payload];
      state.updateMode = UpdateMode.NONE;
    },
    setActiveUser: (state, action: PayloadAction<userModel.IUser>) => {
      state.user = action.payload;
    },
    setUserUpdateMode: (state, action: PayloadAction<UpdateMode>) => {
      state.updateMode = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUsersAsync.pending, (_state, _action) => {
      fetchUsersRequest();
      _state.isLoading = true;
    });
    builder.addCase(fetchUsersAsync.fulfilled, (_state, action) => {
      fetchUsersSuccess(action.payload);
      _state.isLoading = false;
      _state.initialFetch = false;
      _state.users = action.payload;
    });
    builder.addCase(fetchUsersAsync.rejected, (_state, action) => {
      fetchUsersError(action.payload as string);
      _state.isLoading = false;
      _state.errors = action.payload;
    });
  },
});

export const {
  fetchUsersRequest,
  fetchUsersSuccess,
  fetchUsersError,
  editUserSuccess,
  addUserSuccess,
  setActiveUser,
  setUserUpdateMode,
  searchUsersError,
  searchUsersRequest,
  searchUsersSuccess
} = userSlice.actions;

const reducer = userSlice.reducer;

export { reducer as userReducer };

